package com.webwerks.galooliva.data.network;

import com.webwerks.galooliva.data.network.model.AdditionalInfoResponse;
import com.webwerks.galooliva.data.network.model.LoginResponse;
import com.webwerks.galooliva.data.network.model.MeetUpResponse;
import com.webwerks.galooliva.data.network.model.NotificationResponse;
import com.webwerks.galooliva.data.network.model.SingleMeetUpResponse;
import com.webwerks.galooliva.data.network.model.StatusObject;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.QueryMap;

/**
 * Created by webwerks on 15/12/16.
 */

public interface RetroInterface {

    /*POST*/
    @FormUrlEncoded
    @POST("users/postLogin.json")
    Call<LoginResponse> postLogin(@FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST("users/postRegister.json")
    Call<LoginResponse> postRegister(@FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST("users/forgotPassword.json")
    Call<StatusObject> postForgotPassword(@FieldMap(encoded = true) Map<String, String> params);

	@FormUrlEncoded
	@POST("users/postChangePassword.json")
	Call<StatusObject> postChangePassword(@Header("UserAuthToken") String auth_token,@FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST("users/postEditUserProfile.json")
    Call<LoginResponse> postProfileEdit(@Header("UserAuthToken") String auth_token, @FieldMap(encoded = true) Map<String, String> params);

	@FormUrlEncoded
	@POST("user-additional-infos/postAddOrUpdateUserAddtionalInfo.json")
	Call<LoginResponse> postEditAdditionalInfo(@Header("UserAuthToken") String auth_token, @FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST("contact-us/postSendContactUsMessage.json")
    Call<StatusObject> postContactUs(@Header("UserAuthToken") String auth_token, @FieldMap(encoded = true) Map<String, String> params);

	@Multipart
    @POST("sports-meet-up-details/postAddSportsMeetUpDetails.json")
    Call<StatusObject> postCreateMeetUp( @Header("UserAuthToken") String auth_token, @PartMap() Map< String, RequestBody > params, @Part ArrayList<MultipartBody.Part> files);

	@Multipart
	@POST("sports-meet-up-details/postEditSportsMeetUpDetails.json")
	Call<StatusObject> postUpdateMeetUp( @Header("UserAuthToken") String auth_token, @PartMap() Map< String, RequestBody > params, @Part ArrayList<MultipartBody.Part> files);

	@Multipart
	@POST("users/postChangeProfilePicture.json")
	Call<StatusObject> postChangeProfilePicture( @Header("UserAuthToken") String auth_token, @Part MultipartBody.Part file);

    @FormUrlEncoded
    @POST("sports-meet-up-users/postJoinSportsMeetUp.json")
    Call<StatusObject> postJoinSportMeetUp(@Header("UserAuthToken") String auth_token, @FieldMap(encoded = true) Map<String, String> params);

    @FormUrlEncoded
    @POST("sports-meet-up-users/postUnJoinSportsMeetUp.json")
    Call<StatusObject> postUnJoinSportMeetUp(@Header("UserAuthToken") String auth_token, @FieldMap(encoded = true) Map<String, String> params);

	@FormUrlEncoded
	@POST("sports-meet-up-details/postDeleteSportsMeetUpPicture.json")
	Call<StatusObject> postDeleteSportMeetUpPhoto(@Header("UserAuthToken") String auth_token, @FieldMap(encoded = true) Map<String, String> params);

	@FormUrlEncoded
	@POST("users-notifications/postAddOrUpdateNoficationDetails.json")
	Call<StatusObject> postFCMToken(@Header("UserAuthToken") String auth_token, @FieldMap(encoded = true) Map<String, String> params);

	@FormUrlEncoded
	@POST("notification-details/postDeleteNotificationDetails.json")
	Call<StatusObject> postDeleteNotification(@Header("UserAuthToken") String auth_token, @FieldMap(encoded = true) Map<String, String> params);

	@FormUrlEncoded
	@POST("sports-meet-up-users/postApproveJoiningSportsMeetUp.json")
	Call<StatusObject> postAcceptJoinRequest(@Header("UserAuthToken") String auth_token, @FieldMap(encoded = true) Map<String, String> params);

    /*GET*/

    @GET("sports-meet-up-details/getAllSportsMeetUpDetails.json")
    Call<MeetUpResponse> getAllSportsMeetUpDetails(@Header("UserAuthToken") String auth_token,@QueryMap(encoded = true) Map<String, String> params);

    @GET("sports-meet-up-details/getSportsMeetUpDetails.json")
    Call<SingleMeetUpResponse> getSportsMeetUpDetails( @Header("UserAuthToken") String auth_token, @QueryMap(encoded = true) Map<String, String> params);

    @GET("users/getUserProfile.json")
    Call<LoginResponse> getProfileInfo(@Header("UserAuthToken") String auth_token);

    @GET("users/getLogout.json")
    Call<StatusObject> getLogout(@Header("UserAuthToken") String auth_token);

	@GET("user-additional-infos/getUserAddtionalInfo.json")
	Call<AdditionalInfoResponse> getUserAdditionalInfo( @Header("UserAuthToken") String auth_token);

	@GET("notification-details/getAllMyNotifications.json")
	Call<NotificationResponse> getAllNotifications( @Header("UserAuthToken") String auth_token);

}
