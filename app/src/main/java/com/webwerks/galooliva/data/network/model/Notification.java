package com.webwerks.galooliva.data.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 23/1/17.
 */

public class Notification {
	@SerializedName("id")
	private int id;
	@SerializedName("users_notification_id")
	private int users_notification_id;
	@SerializedName("title")
	private String title;
	@SerializedName("subtitle")
	private String subtitle;
	@SerializedName("notification_type")
	private String notification_type;
	@SerializedName("notification_body")
	private String notification_body;
	@SerializedName("is_read")
	private boolean is_read;

	public int getId() {
		return id;
	}

	public void setId( int id ) {
		this.id = id;
	}

	public int getUsers_notification_id() {
		return users_notification_id;
	}

	public void setUsers_notification_id( int users_notification_id ) {
		this.users_notification_id = users_notification_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle( String title ) {
		this.title = title;
	}

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle( String subtitle ) {
		this.subtitle = subtitle;
	}

	public String getNotification_type() {
		return notification_type;
	}

	public void setNotification_type( String notification_type ) {
		this.notification_type = notification_type;
	}

	public String getNotification_body() {
		return notification_body;
	}

	public void setNotification_body( String notification_body ) {
		this.notification_body = notification_body;
	}

	public boolean is_read() {
		return is_read;
	}

	public void setIs_read( boolean is_read ) {
		this.is_read = is_read;
	}
}
