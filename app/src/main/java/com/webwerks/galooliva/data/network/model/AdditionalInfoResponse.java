package com.webwerks.galooliva.data.network.model;

/**
 * Created by webwerks on 13/1/17.
 */

public class AdditionalInfoResponse extends StatusObject{

	private AdditionalInfo data;

	public AdditionalInfo getData() {
		return data;
	}
}
