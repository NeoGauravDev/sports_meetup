package com.webwerks.galooliva.data.network;

import android.app.ActivityManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.view.activities.HomeActivity;
import com.webwerks.galooliva.view.activities.NotificationActivity;

import java.util.List;
import java.util.Map;

public class FBMessagingService extends FirebaseMessagingService {
    //Notification types
    public static final int GROUP_CREATION_NOTIFICATION = 701;
    private static final String TAG = "FBMessagingService";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // TODO: Handle FCM messages here.
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated.
        Log.d(TAG, "From: " + remoteMessage.getFrom());
        //  Log.d(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());

        Map jsonBody = remoteMessage.getData();

        Log.d(TAG, "Notification Body: " + jsonBody.toString());
        Log.d(TAG, "Notification Token: " + SharedPreferenceHelper.getFcmToken( this ));

        String strTitle = null, strSubTitle = null, strContent = null, strType = null;

            strTitle = (String) jsonBody.get("title");
            strSubTitle = (String) jsonBody.get("subtitle");
            strContent = (String) jsonBody.get("body");
            strType = (String) jsonBody.get("type");

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);

        mBuilder.setSmallIcon( R.mipmap.ic_launcher);
        Bitmap bitmap= BitmapFactory.decodeResource(getResources(), R.drawable.ic_logo);
        mBuilder.setLargeIcon(bitmap);
        mBuilder.setContentTitle(strTitle);
        mBuilder.setAutoCancel(true);
        mBuilder.setContentText(strSubTitle);
        mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));

        Intent resultIntent = null;

        //Set All Broadcasts here
        if(strType.equalsIgnoreCase("join_open")){
            resultIntent = new Intent(this, NotificationActivity.class);
        }else if(strType.equalsIgnoreCase("join_closed")){
            resultIntent = new Intent(this, NotificationActivity.class);
        }else if(strType.equalsIgnoreCase("joining_approved")){
	        resultIntent = new Intent(this, NotificationActivity.class);
        }else if(strType.equalsIgnoreCase("updateMeetup")){
	        resultIntent = new Intent(this, NotificationActivity.class);
        }else{
            resultIntent = new Intent(this, HomeActivity.class);
        }
		resultIntent.putExtra( "data","data" );
	   // resultIntent = new Intent(this, NotificationActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(HomeActivity.class);

        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // notificationID allows you to update the notification later on.
        mNotificationManager.notify(GROUP_CREATION_NOTIFICATION, mBuilder.build());
    }

	public boolean checkApp(){
		ActivityManager am = (ActivityManager) this
				.getSystemService(ACTIVITY_SERVICE);

		// get the info from the currently running task
		List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks( 1);

		ComponentName componentInfo = taskInfo.get( 0).topActivity;
		if (componentInfo.getPackageName().equalsIgnoreCase("com.webwerks.galooliva")) {
			return true;
		} else {
			return false;
		}
	}

}