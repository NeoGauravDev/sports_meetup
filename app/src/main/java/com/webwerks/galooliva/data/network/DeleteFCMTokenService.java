package com.webwerks.galooliva.data.network;

import android.app.IntentService;
import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceId;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;

import java.io.IOException;

/**
 * Created by webwerks on 20/1/17.
 */

public class DeleteFCMTokenService extends IntentService {
	public static final String TAG = DeleteFCMTokenService.class.getSimpleName();

	public DeleteFCMTokenService() {
		super(TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		try {
			// Resets Instance ID and revokes all tokens.
			SharedPreferenceHelper.setFcmToken( getApplicationContext(), "" );
			FirebaseInstanceId.getInstance().deleteInstanceId();
			FirebaseInstanceId.getInstance().getToken();
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
}