package com.webwerks.galooliva.data.network.model;

import java.util.ArrayList;

/**
 * Created by webwerks on 27/12/16.
 */

public class SingleMeetUpResponse extends StatusObject {
    private MeetUp data;

	public MeetUp getData() {
		return data;
	}
}
