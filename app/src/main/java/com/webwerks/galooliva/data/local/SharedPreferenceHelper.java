package com.webwerks.galooliva.data.local;

import android.content.Context;
import android.preference.PreferenceManager;

/**
 * Created by Nishant on 23/5/16.
 */
public class SharedPreferenceHelper{

	private static final String KEY_ISLOGIN="pref_isLogin";
	private static final String KEY_USER_AUTH_TOKEN="pref_user_auth_token";
	private static final String KEY_FCM_TOKEN="pref_fcm_token";
	private static final String KEY_IS_FCM_TOKEN_SENT="pref_is_fcm_token_sent";

	public static boolean isSignIn( Context context ){
		return getBooleanPreference( context, KEY_ISLOGIN, false );
	}

	public static void setisSignIn( Context context, boolean value ) {
		setBooleanPreference( context, KEY_ISLOGIN, value );
	}

	public static boolean isFCMSent( Context context ){
		return getBooleanPreference( context, KEY_IS_FCM_TOKEN_SENT, false );
	}

	public static void setisFCMSent( Context context, boolean value ) {
		setBooleanPreference( context, KEY_IS_FCM_TOKEN_SENT, value );
	}

	public static String getAuthToken( Context context ){
		return getStringPreference( context, KEY_USER_AUTH_TOKEN, null );
	}

	public static void setAuthToken( Context context, String value ) {
		setStringPreference( context, KEY_USER_AUTH_TOKEN, value );
	}

	public static String getFcmToken( Context context ){
		return getStringPreference( context, KEY_FCM_TOKEN, null );
	}

	public static void setFcmToken( Context context, String value ) {
		setStringPreference( context, KEY_FCM_TOKEN, value );
	}

	private static void setBooleanPreference( Context context, String key, boolean value ) {
		PreferenceManager.getDefaultSharedPreferences( context ).edit().putBoolean( key, value ).apply();
	}

	private static boolean getBooleanPreference( Context context, String key, boolean defaultValue ) {
		return PreferenceManager.getDefaultSharedPreferences( context ).getBoolean( key, defaultValue );
	}

	private static void setStringPreference( Context context, String key, String value ) {
		PreferenceManager.getDefaultSharedPreferences( context ).edit().putString( key, value ).apply();
	}

	private static String getStringPreference( Context context, String key, String defaultValue ) {
		return PreferenceManager.getDefaultSharedPreferences( context ).getString( key, defaultValue );
	}

	private static int getIntegerPreference( Context context, String key, int defaultValue ) {
		return PreferenceManager.getDefaultSharedPreferences( context ).getInt( key, defaultValue );
	}

	private static void setIntegerPrefrence( Context context, String key, int value ) {
		PreferenceManager.getDefaultSharedPreferences( context ).edit().putInt( key, value ).apply();
	}

	private static boolean setIntegerPrefrenceBlocking( Context context, String key, int value ) {
		return PreferenceManager.getDefaultSharedPreferences( context ).edit().putInt( key, value ).commit();
	}

	private static long getLongPreference( Context context, String key, long defaultValue ) {
		return PreferenceManager.getDefaultSharedPreferences( context ).getLong( key, defaultValue );
	}

	private static void setLongPreference( Context context, String key, long value ) {
		PreferenceManager.getDefaultSharedPreferences( context ).edit().putLong( key, value ).apply();
	}
}
