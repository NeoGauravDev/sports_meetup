package com.webwerks.galooliva.data.network;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.google.gson.Gson;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.network.model.StatusObject;
import com.webwerks.galooliva.data.network.model.User;
import com.webwerks.galooliva.view.activities.BaseActivity;
import com.webwerks.galooliva.view.activities.LoginActivity;

import java.io.IOException;

import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;




/**
 * Created by webwerks on 26/12/16.
 */

public abstract class RetroCallback<T> implements Callback<T>{

    private BaseActivity context;

    public RetroCallback(BaseActivity context) {
        this.context = context;
    }

    public abstract void onSuccess(Call<T> call, Response<T> response);

    public void onError(Call<T> call, Response<T> response){
	    context.dismissProgress();
	    if(response.code() == 401){ // unauthorized access/token expires
		    showTokenExpiresPopup();
		    return;
	    }

        try {
            String res = response.errorBody().string();
            context.oBaseApp.doLog(res);
            StatusObject statusObject = new Gson().fromJson(res, StatusObject.class);
            Toast.makeText(context,  statusObject.getUser_msg(), Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	private void showTokenExpiresPopup() {
		new AlertDialog.Builder( context)
				.setMessage(R.string.msg_token_expires)
				.setCancelable( false )
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						context.finish();
						Intent intent= new Intent( context, LoginActivity.class );
						context.startActivity( intent );
					}
				})
				.show();
	}

	private void onError(Call<T> call, Throwable t){
        context.dismissProgress();
        Toast.makeText(context,  context.getString(R.string.msg_error), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        context.dismissProgress();
        if(response.code() == 200){
            onSuccess(call, response);
        }else{
            onError(call,response);
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        onError(call, t);
    }
}
