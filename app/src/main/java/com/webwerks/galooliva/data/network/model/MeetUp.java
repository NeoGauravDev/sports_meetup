package com.webwerks.galooliva.data.network.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by webwerks on 19/12/16.
 */

public class MeetUp extends MeetUpResponse implements Serializable{
    @SerializedName("id")
    private String id;
    @SerializedName("title")
    private String title;
    @SerializedName("meetup_datetime")
    private String meetup_datetime;
    @SerializedName("description")
    private String description;
    @SerializedName("age_limit_id")
    private int age_limit_id;
    @SerializedName("total_allowed_members")
    private int total_allowed_members;
	@SerializedName("minimum_required_members")
	private int minimum_required_members;
	@SerializedName("total_members_joined")
	private int total_members_joined;
	@SerializedName("meetup_type")
	private String meetup_type;
	@SerializedName("meetup_status")
	private String meetup_status;
	@SerializedName("is_joined")
	private int is_joined;
    @SerializedName("sports_meet_up_photos")
    private ArrayList<SportMeetupPhoto> sports_meet_up_photos;
	@SerializedName("sports_meet_up_users")
	private ArrayList<SportMeetupUser> sports_meet_up_users;

    private String date;
    private String time;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMeetup_datetime() {
        return meetup_datetime;
    }

    public void setMeetup_datetime(String meetup_datetime) {
        this.meetup_datetime = meetup_datetime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getAge_limit_id() {
        return age_limit_id;
    }

    public void setAge_limit_id(int age_limit_id) {
        this.age_limit_id = age_limit_id;
    }

    public int getTotal_allowed_members() {
        return total_allowed_members;
    }

    public void setTotal_allowed_members(int total_allowed_members) {
        this.total_allowed_members = total_allowed_members;
    }

	public int getMinimum_required_members() {
		return minimum_required_members;
	}

	public void setMinimum_required_members( int minimum_required_members ) {
		this.minimum_required_members = minimum_required_members;
	}

	public int getTotal_members_joined() {
		return total_members_joined;
	}

	public void setTotal_members_joined( int total_members_joined ) {
		this.total_members_joined = total_members_joined;
	}

	public String getMeetup_type() {
		return meetup_type;
	}

	public void setMeetup_type( String meetup_type ) {
		this.meetup_type = meetup_type;
	}

	public String getMeetup_status() {
		return meetup_status;
	}

	public void setMeetup_status( String meetup_status ) {
		this.meetup_status = meetup_status;
	}

	public int getIs_joined() {
		return is_joined;
	}

	public void setIs_joined( int is_joined ) {
		this.is_joined = is_joined;
	}

	public ArrayList<SportMeetupPhoto> getSports_meet_up_photos() {
        return sports_meet_up_photos;
    }

    public void setSports_meet_up_photos(ArrayList<SportMeetupPhoto> sports_meet_up_photos) {
        this.sports_meet_up_photos = sports_meet_up_photos;
    }

	public ArrayList< SportMeetupUser > getSports_meet_up_users() {
		return sports_meet_up_users;
	}

	public void setSports_meet_up_users( ArrayList< SportMeetupUser > sports_meet_up_users ) {
		this.sports_meet_up_users = sports_meet_up_users;
	}

	public String getDate() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat formatReturn = new SimpleDateFormat("dd MMM, yyyy");

        Date date = null;
        try {
            date = format.parse(meetup_datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatReturn.format(date);
    }

	public String getDateUnformatted() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat formatReturn = new SimpleDateFormat("yyyy-MM-dd");

		Date date = null;
		try {
			date = format.parse(meetup_datetime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formatReturn.format(date);
	}

    public String getTime() {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        DateFormat formatReturn = new SimpleDateFormat("h:mm a");
        Date date = null;
        try {
            date = format.parse(meetup_datetime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return formatReturn.format(date);
    }

	public String getTimeUnFormatted() {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		DateFormat formatReturn = new SimpleDateFormat("HH:mm");
		Date date = null;
		try {
			date = format.parse(meetup_datetime);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formatReturn.format(date);
	}
}
