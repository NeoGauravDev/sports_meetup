package com.webwerks.galooliva.data.network;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;

/**
 * Created by Vivekanand on 24/08/16.
 */
public class FirebaseIDService extends FirebaseInstanceIdService {

    private static final String TAG = "FirebaseIDService";
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed fcm token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
	    SharedPreferenceHelper.setFcmToken( getApplicationContext(),token );
	    SharedPreferenceHelper.setisFCMSent( getApplicationContext(),false );
    }
}
