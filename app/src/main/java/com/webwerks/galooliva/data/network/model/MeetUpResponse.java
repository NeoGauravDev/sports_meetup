package com.webwerks.galooliva.data.network.model;

import java.util.ArrayList;

/**
 * Created by webwerks on 27/12/16.
 */

public class MeetUpResponse extends StatusObject {

    private ArrayList<MeetUp> data;

    public ArrayList<MeetUp> getData() {
        return data;
    }
}
