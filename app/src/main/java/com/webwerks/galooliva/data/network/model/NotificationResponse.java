package com.webwerks.galooliva.data.network.model;

import java.util.ArrayList;

/**
 * Created by webwerks on 23/1/17.
 */

public class NotificationResponse extends StatusObject {

	private ArrayList<Notification> data;

	public ArrayList<Notification> getData() {
		return data;
	}
}
