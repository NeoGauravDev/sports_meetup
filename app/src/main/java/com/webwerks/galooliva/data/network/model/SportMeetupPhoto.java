package com.webwerks.galooliva.data.network.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by webwerks on 28/12/16.
 */

public class SportMeetupPhoto implements Serializable{
    @SerializedName("id")
    private int id;
    @SerializedName("image_name")
    private String image_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage_name() {
        return image_name;
    }

    public void setImage_name(String image_name) {
        this.image_name = image_name;
    }
}
