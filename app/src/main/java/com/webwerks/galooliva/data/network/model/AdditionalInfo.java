package com.webwerks.galooliva.data.network.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by webwerks on 13/1/17.
 */

public class AdditionalInfo implements Serializable{
	@SerializedName("height")
	private int height;
	@SerializedName("weight")
	private int weight;
	@SerializedName("dominant_hand")
	private String dominant_hand;
	@SerializedName("birth_place")
	private String birth_place;
	@SerializedName("my_interest")
	private String my_interest;
	@SerializedName("goals_or_synonyms")
	private String goals_or_synonyms;
	@SerializedName("user")
	private User user;

	public int getHeight() {
		return height;
	}

	public void setHeight( int height ) {
		this.height = height;
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight( int weight ) {
		this.weight = weight;
	}

	public String getDominant_hand() {
		return dominant_hand;
	}

	public void setDominant_hand( String dominant_hand ) {
		this.dominant_hand = dominant_hand;
	}

	public String getBirth_place() {
		return birth_place;
	}

	public void setBirth_place( String birth_place ) {
		this.birth_place = birth_place;
	}

	public String getMy_interest() {
		return my_interest;
	}

	public void setMy_interest( String my_interest ) {
		this.my_interest = my_interest;
	}

	public String getGoals_or_synonyms() {
		return goals_or_synonyms;
	}

	public void setGoals_or_synonyms( String goals_or_synonyms ) {
		this.goals_or_synonyms = goals_or_synonyms;
	}

	public User getUser() {
		return user;
	}

	public void setUser( User user ) {
		this.user = user;
	}
}
