package com.webwerks.galooliva.data.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by webwerks on 12/1/17.
 */

public class SportMeetupUser{
	@SerializedName( "user" )
	private User user;

	public User getUser() {
		return user;
	}

	public void setUser( User user ) {
		this.user = user;
	}
}
