package com.webwerks.galooliva.data.network.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by webwerks on 22/12/16.
 */

public class User extends LoginResponse implements Serializable{
    @SerializedName("id")
    private String id;
    @SerializedName("first_name")
    private String first_name;
    @SerializedName("last_name")
    private String last_name;
    @SerializedName("dob")
    private String dob;
    @SerializedName("gender")
    private char gender;
    @SerializedName("email")
    private String email;
    @SerializedName("city")
    private String city;
	@SerializedName("country")
	private String country;
    @SerializedName("facebookid")
    private String facebookid;
    @SerializedName("gmailid")
    private String gmailid;
    @SerializedName("profile_pic")
    private String profile_pic;
    @SerializedName("auth_token")
    private String auth_token;
    @SerializedName("full_name")
    private String full_name;

    public User(String id, String first_name, String last_name, String dob, char gender, String email, String city, String facebookid, String gmailid, String profile_pic, String auth_token, String full_name) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.dob = dob;
        this.gender = gender;
        this.email = email;
        this.city = city;
        this.facebookid = facebookid;
        this.gmailid = gmailid;
        this.profile_pic = profile_pic;
        this.auth_token = auth_token;
        this.full_name = full_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

	public String getCountry() {
		return country;
	}

	public void setCountry( String country ) {
		this.country = country;
	}

	public String getFacebookid() {
        return facebookid;
    }

    public void setFacebookid(String facebookid) {
        this.facebookid = facebookid;
    }

    public String getGmailid() {
        return gmailid;
    }

    public void setGmailid(String gmailid) {
        this.gmailid = gmailid;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getAuth_token() {
        return auth_token;
    }

    public void setAuth_token(String auth_token) {
        this.auth_token = auth_token;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }
}


