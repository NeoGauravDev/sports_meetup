package com.webwerks.galooliva;

import android.app.Application;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.stetho.Stetho;
import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.webwerks.galooliva.utils.Constants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by webwerks on 19/12/16.
 */

public class MyApplication extends Application {

    private static MyApplication oBaseApp;
    private static Retrofit retrofit = null;

    @Override
    public void onCreate() {
        super.onCreate();
        oBaseApp = this;
        Stetho.initializeWithDefaults(this);//initialize Stetho
        FacebookSdk.sdkInitialize(getApplicationContext()); // facebook initialization
        AppEventsLogger.activateApp(this);
    }

    public static MyApplication getInstance(){
        return  oBaseApp;
    }

    public static Retrofit getClient() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(20, TimeUnit.SECONDS);
        httpClient.addNetworkInterceptor(new StethoInterceptor());
	    HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
	    loggingInterceptor.setLevel( HttpLoggingInterceptor.Level.BODY );


        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                // Customize the request
                Request request = original.newBuilder()
                        .header("Apikey", "master_token")
                        .header("username", "plusonegaloappuser")
                        .header("password", "plusonegalopwd")
                        .header("Content-Type", "application/x-www-form-urlencoded")
                        .method(original.method(), original.body())
                        .build();
                Response response = chain.proceed( request);
                return response;
            }
        });

        if (retrofit==null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.Urls.BASE_URL)
                    .client(httpClient.build())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public void doToast(String msg){
        Toast.makeText(getApplicationContext(),msg,Toast.LENGTH_LONG).show();
    }

    public void doLog(String msg){
        Log.d("GaloOliva","*** "+ msg);
    }

    public void showDialog(String msg){
        new AlertDialog.Builder(getApplicationContext())
                .setMessage(msg)
                .setNeutralButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .show();
    }
}
