package com.webwerks.galooliva.utils;

/**
 * Created by webwerks on 15/12/16.
 */

public class Constants {

    public static class Urls{
        public static final String BASE_URL = "http://localnews.phpdevelopment.co.in/plusonegalo/api/"; // should contain backslash
	    public static final String MULTIPART_FORM_DATA = "multipart/form-data";
        /* GET webservices */
        /* POST webservices */
    }

    public static class PreferenceKeys{
        public static final String USER_INFO = "user";
        public static final String USER_AUTH_TOKEN = "user_auth_token";
        public static final String IS_LOGGED_IN_USER = "isLoggedIn";
    }
}
