package com.webwerks.galooliva.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Base64;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;


import static com.webwerks.galooliva.utils.Constants.Urls.MULTIPART_FORM_DATA;

/**
 * Created by webwerks on 15/12/16.
 */

public class Util {

    public static void hideKeyboard(Context context, View view) {
        if (view != null) {
            ((InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void closeEditor(Activity activity) {
        View peekDecorView = activity.getWindow().peekDecorView();
        if (peekDecorView != null) {
            ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(peekDecorView.getWindowToken(), 0);
        }
    }

    public static boolean validateEmail(String email) {
        Matcher matcher = Pattern.compile(Patterns.EMAIL_ADDRESS.pattern()) .matcher(email);
        return matcher.find();
    }

    public static void openPermissionSettings(Activity context){
        Intent intent = new Intent();
        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", context.getPackageName(), null);
        intent.setData(uri);
        context.startActivity(intent);
    }

    public static void startInstalledAppDetailsActivity(final Activity context) {
        if (context == null) {
            return;
        }
        final Intent i = new Intent();
        i.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        i.addCategory(Intent.CATEGORY_DEFAULT);
        i.setData(Uri.parse("package:" + context.getPackageName()));
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        i.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        context.startActivity(i);
    }

    public static String getAgeGroupKey(String value){
        HashMap<String, String> mapAgeGroup = new HashMap<>();
        //creating Age group map
        mapAgeGroup.put("0-18","1");
        mapAgeGroup.put("19-35","2");
        mapAgeGroup.put("36-60","3");
        mapAgeGroup.put("61-80","4");
        mapAgeGroup.put("80-100","5");
        return mapAgeGroup.get(value);
    }

	public static String getAgeGroupValues(String key){
		HashMap<String, String> mapAgeGroup = new HashMap<>();
		//creating Age group map
		mapAgeGroup.put("1","0-18");
		mapAgeGroup.put("2","19-35");
		mapAgeGroup.put("3","36-60");
		mapAgeGroup.put("4","61-80");
		mapAgeGroup.put("5","80-100");
		return mapAgeGroup.get(key);
	}

    public static String encodeToBase64(Bitmap image, Bitmap.CompressFormat compressFormat, int quality){
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(compressFormat, quality, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input){
        byte[] decodedBytes = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.length);
    }

	public static RequestBody createPartFromString( String descriptionString) {
		return RequestBody.create(
				MediaType.parse( MULTIPART_FORM_DATA), descriptionString);
	}

}
