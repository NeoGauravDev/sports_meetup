package com.webwerks.galooliva.view.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.webwerks.galooliva.MyApplication;
import com.webwerks.galooliva.R;

public class BaseActivity extends AppCompatActivity {

    public MyApplication oBaseApp;
    private ProgressDialog mProgressDialog;
    private Toolbar toolbar;
    private TextView toolbarTitle;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        oBaseApp = MyApplication.getInstance();
    }

    public void initToolbar(Toolbar toolbar){
        this.toolbar = toolbar;
        toolbarTitle = (TextView) toolbar.getRootView().findViewById(R.id.toolbar_title);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void setToolbarTitle(String title){
        toolbarTitle.setText(title);
    }

    public void setHomeButtonEnabled(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    public void setHomeButtonDisabled(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
    }

    public void showProgress(String msg) {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            dismissProgress();

        mProgressDialog = ProgressDialog.show(this, "", msg);
    }

    public void showProgress() {
        if (mProgressDialog != null && mProgressDialog.isShowing())
            dismissProgress();

        mProgressDialog = ProgressDialog.show(this, "", getString(R.string.msg_progress));
    }

    public void dismissProgress() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
    }

    public void doStackFragment(Fragment fragment, FrameLayout layout , String fragmentName){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(layout.getId(), fragment, fragmentName);
        ft.addToBackStack(null);
        ft.commit();
    }

    public void doReplaceFragment(Fragment fragment, FrameLayout layout , String fragmentName){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(layout.getId(), fragment);
        ft.commit();
    }
}
