package com.webwerks.galooliva.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.MeetUpResponse;
import com.webwerks.galooliva.view.activities.HomeActivity;
import com.webwerks.galooliva.view.adapters.GroupsRVAdapter;
import com.webwerks.galooliva.data.network.model.MeetUp;
import com.webwerks.galooliva.view.adapters.MyMeetUpsRVAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by webwerks on 21/12/16.
 */

public class GroupsFragment extends Fragment {

    private HomeActivity context; // context variable represents Activity scope
    private View rootView;

    private GroupsRVAdapter groupsRVAdapter;
    private RecyclerView recyclerView;
    private ArrayList<MeetUp> groupList = new ArrayList<>();
	private MeetUpResponse meetUpResponse;

	@Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_groups, container,false);
        init();
        return rootView;
    }

    private void init() {
        context = (HomeActivity) getActivity();
        initViews();
        initValues();
    }

    private void initViews() {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.GroupsFragment_RV_groups);
    }

    private void initValues() {
        callGetAllSportsMeetups();
    }


    @Override
    public void onResume() {
        super.onResume();
        context.setToolbarTitle(getString(R.string.groups));
        context.setHomeButtonEnabled();
        context.setselectedTab(1); // position of groups
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_filter).setVisible(false);
    }

	private void callGetAllSportsMeetups() {
		context.showProgress();
		RetroInterface          apiService = context.oBaseApp.getClient().create( RetroInterface.class);
		Map<String, String> mapFields  = new HashMap<>();
		mapFields.put( "own","yes" );
		context.oBaseApp.doLog("FieldMap: "+ mapFields);
		Call<MeetUpResponse> call = apiService.getAllSportsMeetUpDetails( SharedPreferenceHelper.getAuthToken( context),mapFields);
		call.enqueue(new RetroCallback<MeetUpResponse>( context) {
			@Override
			public void onSuccess(Call<MeetUpResponse> call, Response<MeetUpResponse> response) {
				try{
					meetUpResponse = response.body();
					if(meetUpResponse.getData() != null){
						groupsRVAdapter = new GroupsRVAdapter( context, meetUpResponse.getData());
						RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
						recyclerView.setLayoutManager(mLayoutManager);
						recyclerView.setItemAnimator(new DefaultItemAnimator());
						recyclerView.setAdapter(groupsRVAdapter);
					}
				}catch (Exception e){
					e.printStackTrace();
				}
			}
		});
	}
}
