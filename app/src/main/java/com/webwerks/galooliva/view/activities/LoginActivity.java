package com.webwerks.galooliva.view.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.LoginResponse;
import com.webwerks.galooliva.utils.NetworkUtil;
import com.webwerks.galooliva.utils.Util;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Response;

@RuntimePermissions
public class LoginActivity extends BaseActivity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
	private LoginActivity context; // context variable represents Activity scope
	private TextView      tvForgotPassword, tvSignUp;
	private EditText edtEmail, edtPassword;
	private Button    btnLogin;
	private Intent    intent;
	private ImageView imgFacebook, imgGoogle;

	//facebook
	private CallbackManager callbackManager;
	//Google
	private GoogleApiClient mGoogleApiClient;
	private int RC_SIGN_IN = 1010;
	private Location mLastLocation;

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_login );
		init();
	}

	private void init() {
		context = this;
		iniViews();
		initValues();
	}

	private void iniViews() {
		tvForgotPassword = ( TextView ) findViewById( R.id.LoginActivity_TV_forgotpassword );
		tvSignUp = ( TextView ) findViewById( R.id.LoginActivity_TV_signup );
		edtEmail = ( EditText ) findViewById( R.id.LoginActivity_ET_email );
		edtPassword = ( EditText ) findViewById( R.id.LoginActivity_ET_password );
		btnLogin = ( Button ) findViewById( R.id.LoginActivity_BTN_login );
		imgFacebook = ( ImageView ) findViewById( R.id.LoginActivity_IMG_facebook );
		imgGoogle = ( ImageView ) findViewById( R.id.LoginActivity_IMG_google );
	}

	private void initValues() {
		tvForgotPassword.setOnClickListener( this );
		tvSignUp.setOnClickListener( this );
		btnLogin.setOnClickListener( this );
		imgFacebook.setOnClickListener( this );
		imgGoogle.setOnClickListener( this );
	}

	@Override
	public void onClick( View view ) {
		switch ( view.getId() ) {
			case R.id.LoginActivity_TV_forgotpassword:
				intent = new Intent( context, ForgotPasswordActivity.class );
				startActivity( intent );
				break;
			case R.id.LoginActivity_TV_signup:
				intent = new Intent( context, RegisterActivity.class );
				startActivity( intent );
				break;
			case R.id.LoginActivity_BTN_login:
				validateInputs();
				break;
			case R.id.LoginActivity_IMG_facebook:
				if ( NetworkUtil.isConnected( context ) ) {
					callFacebookLogin();
				}
				else {
					oBaseApp.doToast( getString( R.string.msg_noInternet ) );
				}
				break;
			case R.id.LoginActivity_IMG_google:
				if ( NetworkUtil.isConnected( context ) ) {
					LoginActivityPermissionsDispatcher.showGoogleWithCheck( this );
				}
				else {
					oBaseApp.doToast( getString( R.string.msg_noInternet ) );
				}
				break;
		}
		Util.closeEditor( context );
	}

	private void validateInputs() {
		if ( NetworkUtil.isConnected( context ) ) {
			if ( edtEmail.getText().toString().trim().length() > 0 ) {
				if ( edtPassword.getText().toString().trim().length() > 0 ) {
					callLoginService();
				}
				else {
					oBaseApp.doToast( "Please enter valid password." );
				}
			}
			else {
				oBaseApp.doToast( "Please enter valid email id." );
			}
		}
		else {
			oBaseApp.doToast( getString( R.string.msg_noInternet ) );
		}
	}

	private void callLoginService() {
		showProgress();
		RetroInterface apiService = oBaseApp.getClient().create( RetroInterface.class );

		Map< String, String > mapFields = new HashMap<>();
		mapFields.put( "email", edtEmail.getText().toString().trim() );
		mapFields.put( "password", edtPassword.getText().toString().trim() );
		oBaseApp.doLog( "FieldMap: " + mapFields );

		Call< LoginResponse > call = apiService.postLogin( mapFields );
		call.enqueue( new RetroCallback< LoginResponse >( context ) {
			@Override
			public void onSuccess( Call< LoginResponse > call, Response< LoginResponse > response ) {
				try {
					LoginResponse loginResponse = response.body();
					SharedPreferenceHelper.setisFCMSent( context,false );
					SharedPreferenceHelper.setAuthToken( context, loginResponse.getData().getAuth_token() );
					SharedPreferenceHelper.setisSignIn( context, true );
					intent = new Intent( context, HomeActivity.class );
					startActivity( intent );
					finish();
				}
				catch ( Exception e ) {
					e.printStackTrace();
				}
			}
		} );
	}

	private void callGoogleLogin() {
		GoogleSignInOptions gso = new GoogleSignInOptions.Builder( GoogleSignInOptions.DEFAULT_SIGN_IN )
				.requestEmail()
				.requestId()
				.requestProfile()
				.build();

		mGoogleApiClient = new GoogleApiClient.Builder( this )
				.addApi(LocationServices.API)
				.enableAutoManage( this /* FragmentActivity */, this /* OnConnectionFailedListener */ )
				.addApi( Auth.GOOGLE_SIGN_IN_API, gso )
				.addConnectionCallbacks(this)
				.build();

		mGoogleApiClient.connect();

		Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent( mGoogleApiClient );
		startActivityForResult( signInIntent, RC_SIGN_IN );
	}

	public void callFacebookLogin() {
		callbackManager = CallbackManager.Factory.create();

		// Set permissions
		LoginManager.getInstance().logInWithReadPermissions( this, Arrays.asList( "email", "user_photos", "public_profile", "user_location" ) );

		LoginManager.getInstance().registerCallback( callbackManager,
		                                             new FacebookCallback< LoginResult >() {
			                                             @Override
			                                             public void onSuccess( LoginResult loginResult ) {
				                                             Bundle parameters = new Bundle();
				                                             parameters.putString( "fields", "id,name,last_name,link,email,picture,location" );
				                                             System.out.println( "Success" );
				                                             GraphRequest request = GraphRequest.newMeRequest(
						                                             loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
							                                             @Override
							                                             public void onCompleted( JSONObject json, GraphResponse response ) {
								                                             if ( response.getError() != null ) {
									                                             oBaseApp.doToast( getString( R.string.msg_error ) );
								                                             }
								                                             else {
									                                             String jsonresult = String.valueOf( json );
									                                             oBaseApp.doLog( "Facebook JSON Result : " + jsonresult );
									                                             if ( json.optString( "email" ).length() > 0 ) {
										                                             callGoogleFacebookLoginService( json.optString( "name" ), json.optString( "email" ), json.optString( "id" ), "" );
									                                             }
									                                             else {
										                                             Toast.makeText( context, "Because of Private Policies, can't login using facebook.Please try another login.", Toast.LENGTH_SHORT ).show();
									                                             }
								                                             }
							                                             }
						                                             } );
				                                             request.setParameters( parameters );
				                                             request.executeAsync();
			                                             }

			                                             @Override
			                                             public void onCancel() {
				                                             oBaseApp.doLog( "On cancel" );
			                                             }

			                                             @Override
			                                             public void onError( FacebookException error ) {
				                                             oBaseApp.doLog( error.toString() );
				                                             Toast.makeText( context, "Can not login using facebook.Please try another login.", Toast.LENGTH_LONG ).show();
			                                             }
		                                             } );
	}

	protected void onActivityResult( int requestCode, int resultCode, Intent data ) {
		super.onActivityResult( requestCode, resultCode, data );
		if ( requestCode == RC_SIGN_IN ) {
			GoogleSignInResult result     = Auth.GoogleSignInApi.getSignInResultFromIntent( data );
			int                statusCode = result.getStatus().getStatusCode();
			oBaseApp.doLog( "Google status code: " + statusCode );
			handleSignInResult( result );
		}
		else {
			callbackManager.onActivityResult( requestCode, resultCode, data );
		}
	}

	private void handleSignInResult( GoogleSignInResult result ) {
		oBaseApp.doLog( "Google SignIn Result: " + result.isSuccess() );
		if ( result.isSuccess() ) {
			// Signed in successfully, show authenticated UI.
			GoogleSignInAccount acct = result.getSignInAccount();
			oBaseApp.doLog( acct.getEmail() + "   " + acct.getDisplayName() + "  " + acct.getId() );

			final String str_email = acct.getEmail();
			final String str_name  = acct.getDisplayName();
			final String str_id    = acct.getId();

			if ( str_email.length() > 0 ) {
				//callGoogleFacebookLoginService( str_name, str_email, "", str_id );
			}
			else {
				oBaseApp.doToast( "Can not login using Google.Please try another login." );
			}
		}
		else {
			// Signed out, show unauthenticated UI.

		}
	}

	private void callGoogleFacebookLoginService( String firstName, String email, String facebookId, String gmailId ) {
		showProgress();
		RetroInterface apiService = oBaseApp.getClient().create( RetroInterface.class );

		Map< String, String > mapFields = new HashMap<>();
		mapFields.put( "first_name", firstName );
		mapFields.put( "email", email );
		if ( facebookId.equalsIgnoreCase( "" ) ) {
			mapFields.put( "gmailid", gmailId );
		}
		else {
			mapFields.put( "facebookid", facebookId );
		}
		oBaseApp.doLog( "FieldMap: " + mapFields );

		Call< LoginResponse > call = apiService.postRegister( mapFields );
		call.enqueue( new RetroCallback< LoginResponse >( context ) {
			@Override
			public void onSuccess( Call< LoginResponse > call, Response< LoginResponse > response ) {
				try {
					LoginResponse loginResponse = response.body();

					SharedPreferenceHelper.setAuthToken( context, loginResponse.getData().getAuth_token() );
					SharedPreferenceHelper.setisSignIn( context, true );
					intent = new Intent( context, HomeActivity.class );
					startActivity( intent );
					finish();
				}
				catch ( Exception e ) {
					e.printStackTrace();
				}
			}
		} );
	}

	@Override
	public void onPause() {
		super.onPause();
		if ( mGoogleApiClient != null ) {
			mGoogleApiClient.stopAutoManage( context );
			mGoogleApiClient.disconnect();
		}
	}

	@Override
	public void onConnected( @Nullable Bundle bundle ) {
		if ( ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission( this, Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED ) {
			return;
		}
		mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
				mGoogleApiClient );
		if (mLastLocation != null) {
			oBaseApp.doToast( ""+String.valueOf(mLastLocation.getLatitude()) +"   "+ String.valueOf(mLastLocation.getLongitude()));
		}
	}

	@Override
	public void onConnectionSuspended( int i ) {

	}

	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

	}

	@Override
	public void onRequestPermissionsResult( int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults ) {
		super.onRequestPermissionsResult( requestCode, permissions, grantResults );
		LoginActivityPermissionsDispatcher.onRequestPermissionsResult( this, requestCode, grantResults );
	}

	@NeedsPermission( { Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION } )
	void showGoogle() {
		callGoogleLogin();
	}
}