package com.webwerks.galooliva.view.adapters;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.utils.CircleTransform;
import com.webwerks.galooliva.view.activities.DetailsActivity;
import com.webwerks.galooliva.view.activities.HomeActivity;
import com.webwerks.galooliva.data.network.model.MeetUp;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by webwerks on 19/12/16.
 */

public class MeetUpsRVAdapter extends RecyclerView.Adapter<MeetUpsRVAdapter.MyViewHolder> {

    private ArrayList<MeetUp> meetUpArrayList;
    private HomeActivity context;

    public MeetUpsRVAdapter(HomeActivity context, ArrayList<MeetUp> meetUpArrayList){
        this.context = context;
        this.meetUpArrayList = meetUpArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_meetup_rv_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

	    if(meetUpArrayList.get( position ).getIs_joined() == 1){
		    holder.itemView.setBackgroundColor( Color.parseColor("#dbdbdb") );
	    }else{
		    holder.itemView.setBackgroundColor( Color.WHITE );
	    }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =new Intent(context, DetailsActivity.class);
                intent.putExtra("meetup", (Serializable) meetUpArrayList.get(position));
                intent.putExtra("isFromGroup", false);
                context.startActivity(intent);
            }
        });

        holder.tvTitle.setText(meetUpArrayList.get(position).getTitle());
	    if(meetUpArrayList.get(position).getMeetup_datetime()!=null){
		    holder.tvTime.setText(meetUpArrayList.get(position).getTime());
		    holder.tvDate.setText(meetUpArrayList.get(position).getDate());
	    }else{
		    holder.tvDate.setText("");
		    holder.tvTime.setText("");
	    }


        holder.tvGroupMembers.setText(meetUpArrayList.get(position).getTotal_allowed_members()+"");
        if(meetUpArrayList.get(position).getSports_meet_up_photos().size()>0){
            Picasso.with(context).load(meetUpArrayList.get(position).getSports_meet_up_photos().get(0).getImage_name()).resize(90,90).centerCrop().transform(new CircleTransform()).into(holder.imgMain);
        }else{
            Picasso.with(context).load(R.mipmap.ic_launcher).resize(90,90).centerCrop().transform(new CircleTransform()).into(holder.imgMain);
        }
    }

    @Override
    public int getItemCount() {
        return meetUpArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvDate, tvTime, tvGroupMembers;
        public ImageView imgMain;

        public MyViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.MeetUpsRVadapter_TV_title);
            tvDate = (TextView) view.findViewById(R.id.MeetUpsRVadapter_TV_date);
            tvTime = (TextView) view.findViewById(R.id.MeetUpsRVadapter_TV_time);
            tvGroupMembers = (TextView) view.findViewById(R.id.MeetUpsRVadapter_TV_groupMembers);
            imgMain = (ImageView) view.findViewById(R.id.MeetUpsRVAdapter_IMG);
        }
    }
}
