package com.webwerks.galooliva.view.activities;

import android.content.Intent;
import android.os.Handler;
import android.os.Bundle;

import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.utils.Constants;

public class SplashActivity extends BaseActivity {

    private static int SPLASH_TIME_OUT = 3000;
    private final SplashActivity context = this; // context represents the activity scope

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(SharedPreferenceHelper.isSignIn(context)){
                    Intent i = new Intent(context, HomeActivity.class);
                    startActivity(i);
                }else {
                    Intent i = new Intent(context, LoginActivity.class);
                    startActivity(i);
                }
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}
