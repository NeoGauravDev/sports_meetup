package com.webwerks.galooliva.view.activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.MeetUp;
import com.webwerks.galooliva.data.network.model.SingleMeetUpResponse;
import com.webwerks.galooliva.data.network.model.StatusObject;
import com.webwerks.galooliva.data.network.model.User;
import com.webwerks.galooliva.utils.CircleTransform;
import com.webwerks.galooliva.utils.Util;
import com.webwerks.galooliva.view.adapters.DetailsViewPagerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

public class DetailsActivity extends BaseActivity {

    private DetailsActivity context; // context variable represents Activity scope
    private Toolbar toolbar;
    private TextView tvTitle, tvDesc, tvAllowedMembers, tvAgeGroup, tvMinNo, tvMembersJoined ,tvDate, tvTime;
	private Spinner spnMembers;
    private Button    btnJoin;
    private MeetUp    meetUp;
	private ImageView img;
	private ViewPager viewPager;
	private boolean isFromGroup = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        init();
    }

    private void init() {
        context = this;
		isFromGroup =getIntent().getExtras().getBoolean( "isFromGroup" ); // to reuse this screen as Group details, using boolean variable
        initViews();
        initValues();
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        initToolbar(toolbar);
        setHomeButtonEnabled();

	    //img = (ImageView) findViewById( R.id.DetailsActivity_IMG );
	    viewPager = (ViewPager ) findViewById( R.id.DetailsActivity_VP );
        tvDate = (TextView) findViewById(R.id.DetailsActivity_TV_date);
        tvTime = (TextView) findViewById(R.id.DetailsActivity_TV_time);

        tvTitle = (TextView) findViewById(R.id.DetailsActivity_TV_title);
        tvDesc = (TextView) findViewById(R.id.DetailsActivity_TV_description);
        tvAllowedMembers = (TextView) findViewById(R.id.DetailsActivity_TV_allowed_members);
        tvAgeGroup = (TextView) findViewById(R.id.DetailsActivity_TV_age_group);
        tvMinNo = (TextView) findViewById(R.id.DetailsActivity_TV_min_no_members);
        tvMembersJoined = (TextView) findViewById(R.id.DetailsActivity_TV_total_members_joined);
        btnJoin = (Button) findViewById(R.id.DetailsActivity_BTN_join_unjoin);
	    spnMembers = (Spinner) findViewById( R.id.DetailsActivity_SPN_name_of_members );


    }

    private void initValues() {
        meetUp = (MeetUp) getIntent().getExtras().getSerializable("meetup");

	    if(isFromGroup){
		    setToolbarTitle(getString(R.string.groupDetails));
		    btnJoin.setText( "Add People" );
	    }else{
		    setToolbarTitle(getString(R.string.details));
		    if(meetUp.getIs_joined() == 0){ // not joined
			    btnJoin.setText( "Join" );
		    }else{
			    btnJoin.setText( "Un-Join" );
		    }
	    }
	    callGetSportMeetupDetailsService();
	   /* if(meetUp.getSports_meet_up_photos().size()>0){
		    Picasso.with( context).load( meetUp.getSports_meet_up_photos().get(0).getImage_name()).resize( 300,400 ).centerCrop().into(img);
	    }else{
		    Picasso.with(context).load(R.mipmap.ic_launcher).centerInside().resize( 100,100 ).into(img);
	    }*/

        btnJoin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((Button)view).getText().toString().equalsIgnoreCase("join")){
                    callJoinSportMeetupService();
                }else if(((Button)view).getText().toString().equalsIgnoreCase("Un-Join")){
                    callUnJoinSportMeetupService();
                }else{
	                invitePeoplePopup();
                }
            }
        });
    }

	private void callGetSportMeetupDetailsService() {
		//context.showProgress();

		Map<String, String> mapFields = new HashMap<>();
		mapFields.put("id", meetUp.getId());
		context.oBaseApp.doLog("FieldMap: "+ mapFields);

		RetroInterface              apiService = context.oBaseApp.getClient().create(RetroInterface.class);
		Call<SingleMeetUpResponse> call       = apiService.getSportsMeetUpDetails( SharedPreferenceHelper.getAuthToken( context), mapFields);
		call.enqueue(new RetroCallback<SingleMeetUpResponse>(context) {
			@Override
			public void onSuccess(Call<SingleMeetUpResponse> call, Response<SingleMeetUpResponse> response) {
				MeetUp               meetUp              = response.body().getData();
				tvDate.setText(meetUp.getDate());
				tvTime.setText(meetUp.getTime());
				tvTitle.setText(meetUp.getTitle());
				tvDesc.setText(meetUp.getDescription());
				tvAllowedMembers.setText(meetUp.getTotal_allowed_members()+"");
				tvAgeGroup.setText(Util.getAgeGroupValues(meetUp.getAge_limit_id()+""));
				tvMinNo.setText(meetUp.getMinimum_required_members()+"");
				tvMembersJoined.setText(meetUp.getTotal_members_joined()+"");

				viewPager.setAdapter( new DetailsViewPagerAdapter( context,meetUp.getSports_meet_up_photos() ) );
				ArrayList<String> members =new ArrayList< String >(  );
				for (int i =0; i< meetUp.getSports_meet_up_users().size();i++){
					members.add( meetUp.getSports_meet_up_users().get( i ).getUser().getFull_name() );
				}
				String [] arrMembers = members.toArray(new String[members.size()]);
				ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>( context, android.R.layout.simple_spinner_item, arrMembers); //selected item will look like a spinner set from XML
				spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				spnMembers.setAdapter(spinnerArrayAdapter);
			}
		});
	}

    private void callJoinSportMeetupService() {
        context.showProgress();

        Map<String, String> mapFields = new HashMap<>();
        mapFields.put("sports_meet_up_detail_id", meetUp.getId());
        context.oBaseApp.doLog("FieldMap: "+ mapFields);

        RetroInterface apiService = context.oBaseApp.getClient().create(RetroInterface.class);
        Call<StatusObject> call = apiService.postJoinSportMeetUp(SharedPreferenceHelper.getAuthToken(context),mapFields);
        call.enqueue(new RetroCallback<StatusObject>(context) {
            @Override
            public void onSuccess(Call<StatusObject> call, Response<StatusObject> response) {
                try {
                    btnJoin.setText("Un-Join");
	                callGetSportMeetupDetailsService();
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void callUnJoinSportMeetupService() {
        context.showProgress();

        Map<String, String> mapFields = new HashMap<>();
        mapFields.put("sports_meet_up_detail_id", meetUp.getId());
        context.oBaseApp.doLog("FieldMap: "+ mapFields);

        RetroInterface apiService = context.oBaseApp.getClient().create(RetroInterface.class);
        Call<StatusObject> call = apiService.postUnJoinSportMeetUp(SharedPreferenceHelper.getAuthToken(context),mapFields);
        call.enqueue(new RetroCallback<StatusObject>(context) {
            @Override
            public void onSuccess(Call<StatusObject> call, Response<StatusObject> response) {
                try {
                    btnJoin.setText("Join");
	                callGetSportMeetupDetailsService();
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

	private void invitePeoplePopup() {
		final Dialog dialog = new Dialog( context);
		dialog.requestWindowFeature( Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.popup_invite_people);
		Button dialogButton = (Button) dialog.findViewById(R.id.Popup_BTN_send);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}
}
