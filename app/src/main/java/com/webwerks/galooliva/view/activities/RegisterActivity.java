package com.webwerks.galooliva.view.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.LoginResponse;
import com.webwerks.galooliva.utils.Constants;
import com.webwerks.galooliva.utils.NetworkUtil;
import com.webwerks.galooliva.utils.Util;
import com.webwerks.galooliva.view.adapters.GoogleAutoCompleteAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseActivity implements View.OnClickListener {
    private RegisterActivity context; // context variable represents Activity scope
    private TextView tvLogin;
    private EditText edtFirstName, edtLastName,edtBirthDate, edtGender, edtEmail, edtPassword, edtCity, edtCountry;
    private Button btnRegister;
    private DatePickerDialog datePickerDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
    }

    private void init() {
        context = this;
        iniViews();
        initValues();
    }

    private void iniViews() {
        edtFirstName = (EditText) findViewById(R.id.RegisterActivity_ET_firstname);
        edtLastName = (EditText) findViewById(R.id.RegisterActivity_ET_lastname);
        edtBirthDate = (EditText) findViewById(R.id.RegisterActivity_ET_birthdate);
        edtGender = (EditText) findViewById(R.id.RegisterActivity_ET_gender);
        edtEmail = (EditText) findViewById(R.id.RegisterActivity_ET_email);
        edtPassword = (EditText) findViewById(R.id.RegisterActivity_ET_password);
        edtCity = (EditText) findViewById(R.id.RegisterActivity_ET_city);
	    edtCountry = (EditText) findViewById(R.id.RegisterActivity_ET_country);

        tvLogin = (TextView) findViewById(R.id.RegisterActivity_TV_login);
        btnRegister = (Button) findViewById(R.id.RegisterActivity_BTN_register);

	    AutoCompleteTextView autoCompView = (AutoCompleteTextView) findViewById( R.id.RegisterActivity_ACTXT_city);

	    autoCompView.setAdapter(new GoogleAutoCompleteAdapter( this, R.layout.layout_google_place_list_item));
	    autoCompView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
		    @Override
		    public void onItemClick( AdapterView< ? > adapterView, View view, int i, long l ) {
			    String str = (String) adapterView.getItemAtPosition(i);
			    oBaseApp.doToast( str );
		    }
	    } );
    }

    private void initValues() {
        tvLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);
        edtBirthDate.setOnClickListener(this);
        edtGender.setOnClickListener(this);
    }

    private void callRegisterService() {
        showProgress();
        RetroInterface apiService = oBaseApp.getClient().create(RetroInterface.class);

        Map<String, String> mapFields = new HashMap<>();
        mapFields.put("first_name", edtFirstName.getText().toString().trim());
        mapFields.put("last_name", edtLastName.getText().toString().trim());
        mapFields.put("dob", edtBirthDate.getText().toString().trim());
        mapFields.put("gender", (edtGender.getText().toString().trim().equalsIgnoreCase("male"))?"M":"F");
        mapFields.put("email", edtEmail.getText().toString().trim());
        mapFields.put("password", edtPassword.getText().toString().trim());
        mapFields.put("city", edtCity.getText().toString().trim());
        mapFields.put("country", edtCountry.getText().toString().trim());
        oBaseApp.doLog("FieldMap: "+ mapFields);
        
        Call<LoginResponse> call = apiService.postRegister(mapFields);
        call.enqueue(new RetroCallback<LoginResponse>(context) {
            @Override
            public void onSuccess(Call<LoginResponse> call, Response<LoginResponse> response) {
                oBaseApp.doToast(response.body().getUser_msg());
	            finish();
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.RegisterActivity_TV_login:
                finish();
                break;
            case R.id.RegisterActivity_BTN_register:
                validateInputs();
                break;
            case R.id.RegisterActivity_ET_birthdate:
                setDate();
                break;
            case R.id.RegisterActivity_ET_gender:
                setGender();
                break;
        }
        Util.closeEditor(context);
    }

    private void validateInputs() {
        if(NetworkUtil.isConnected(context)){
            if(edtFirstName.getText().toString().trim().length()>2){
                if(edtLastName.getText().toString().trim().length()>2){
                    if(edtBirthDate.getText().toString().trim().length()>0){
                        if(edtGender.getText().toString().trim().length()>0){
                            if(Util.validateEmail(edtEmail.getText().toString().trim())){
                                if(edtPassword.getText().toString().trim().length()>5){
                                    if(edtCity.getText().toString().trim().length()>0){
	                                    if(edtCountry.getText().toString().trim().length()>0){
		                                    callRegisterService();
	                                    }else{
		                                    oBaseApp.doToast("Please enter country.");
	                                    }
                                    }else{
                                        oBaseApp.doToast("Please enter city.");
                                    }
                                }else{
                                    oBaseApp.doToast("Password needs to be at least 6 characters long.");
                                }
                            }else{
                                oBaseApp.doToast("Please enter valid email.");
                            }
                        }else{
                            oBaseApp.doToast("Please select gender.");
                        }
                    }else{
                        oBaseApp.doToast("Please set birth date.");
                    }
                }else{
                    oBaseApp.doToast("Last name needs to be at least 3 characters long");
                }
            }else{
                oBaseApp.doToast("First name needs to be at least 3 characters long.");
            }
        }else{
            oBaseApp.doToast(getString(R.string.msg_noInternet));
        }
    }

    public void setGender(){
        final CharSequence[] gender = {"Male","Female"};
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Select Gender");
        alert.setSingleChoiceItems(gender,-1, new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                edtGender.setText(gender[which]+"");
                dialog.dismiss();
            }
        });
        alert.show();
    }

    public void setDate(){
        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                edtBirthDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }
}
