package com.webwerks.galooliva.view.fragments;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.squareup.picasso.Picasso;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.MeetUp;
import com.webwerks.galooliva.data.network.model.SingleMeetUpResponse;
import com.webwerks.galooliva.data.network.model.StatusObject;
import com.webwerks.galooliva.utils.NetworkUtil;
import com.webwerks.galooliva.utils.Util;
import com.webwerks.galooliva.view.activities.HomeActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * Created by webwerks on 21/12/16.
 */

@RuntimePermissions
public class CreateMeetupFragment extends Fragment implements View.OnClickListener {

	private static final int REQUEST_SELECT_PHOTO_GALLERY = 1002 ;
	String mCurrentPhotoPath;
	private HomeActivity context; // context variable represents Activity scope
	private View         rootView;
	private EditText     edtTitle, edtDate, edtTime, edtDescription, edtAgeLimit, edtAllowedMembers, edtMinNoOfMembers;
	private ImageView imgPic1, imgPic2, imgPic3, imgPic4, imgPic5;
	private ImageView imgPicDelete1, imgPicDelete2, imgPicDelete3, imgPicDelete4, imgPicDelete5;
	private Button           btnCreate;
	private RadioGroup radioGroup;
	private DatePickerDialog datePickerDialog;
	private int imgPos = 0;
	private ArrayList<MultipartBody.Part> imageArrayList;
	private HashMap<Integer ,String>      imagePathMap; // using it as main Map to handle delete and add pic functionality
	private MeetUp                        meetUp; // used while updating meetup

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_create_sport_meetup, container,false);
		init();
		return rootView;
	}

	private void init() {
		context = (HomeActivity) getActivity();
		meetUp = ( MeetUp ) getArguments().getSerializable( "meetup" );
		initViews();
		initValues();
	}

	private void initViews() {
		edtTitle = (EditText) rootView.findViewById(R.id.CreateMeetupFragment_ET_title);
		edtDate = (EditText) rootView.findViewById(R.id.CreateMeetupFragment_ET_date);
		edtTime = (EditText) rootView.findViewById(R.id.CreateMeetupFragment_ET_time);
		edtDescription = (EditText) rootView.findViewById(R.id.CreateMeetupFragment_ET_description);
		edtAgeLimit = (EditText) rootView.findViewById(R.id.CreateMeetupFragment_ET_age_group);
		edtAllowedMembers = (EditText) rootView.findViewById(R.id.CreateMeetupFragment_ET_allowed_members);
		edtMinNoOfMembers = (EditText) rootView.findViewById(R.id.CreateMeetupFragment_ET_min_no_members);

		imgPic1 = (ImageView) rootView.findViewById(R.id.CreateMeetupFragment_IMG_pic1);
		imgPic2 = (ImageView) rootView.findViewById(R.id.CreateMeetupFragment_IMG_pic2);
		imgPic3 = (ImageView) rootView.findViewById(R.id.CreateMeetupFragment_IMG_pic3);
		imgPic4 = (ImageView) rootView.findViewById(R.id.CreateMeetupFragment_IMG_pic4);
		imgPic5 = (ImageView) rootView.findViewById(R.id.CreateMeetupFragment_IMG_pic5);

		imgPicDelete1 = (ImageView) rootView.findViewById(R.id.CreateMeetupFragment_IMG_pic_delete1);
		imgPicDelete2 = (ImageView) rootView.findViewById(R.id.CreateMeetupFragment_IMG_pic_delete2);
		imgPicDelete3 = (ImageView) rootView.findViewById(R.id.CreateMeetupFragment_IMG_pic_delete3);
		imgPicDelete4 = (ImageView) rootView.findViewById(R.id.CreateMeetupFragment_IMG_pic_delete4);
		imgPicDelete5 = (ImageView) rootView.findViewById(R.id.CreateMeetupFragment_IMG_pic_delete5);

		btnCreate = (Button) rootView.findViewById(R.id.CreateMeetupFragment_BTN_create);
		radioGroup = (RadioGroup) rootView.findViewById( R.id.CreateMeetupFragment_RG_meetup_status );

		imagePathMap =new HashMap<>(  );
		if(meetUp!= null){ //edit
			btnCreate.setText( "Update" );
			edtTitle.setText( meetUp.getTitle() );
			edtDate.setText( meetUp.getDateUnformatted() );
			edtTime.setText( meetUp.getTimeUnFormatted() );
			edtDescription.setText( meetUp.getDescription() );
			edtAgeLimit.setText( Util.getAgeGroupValues(meetUp.getAge_limit_id()+"") );
			edtAllowedMembers.setText( meetUp.getTotal_allowed_members()+"");
			edtMinNoOfMembers.setText( meetUp.getMinimum_required_members()+"" );
			if(meetUp.getMeetup_type().equalsIgnoreCase( "open" )){
				((RadioButton)radioGroup.getChildAt( 0 )).setChecked( true );
			}else{
				((RadioButton)radioGroup.getChildAt( 1 )).setChecked( true );
			}

			//imagePathMap =new HashMap<>(  );
			context.oBaseApp.doLog("web photos size: "+ meetUp.getSports_meet_up_photos().size() );
			for ( int i = 0; i<meetUp.getSports_meet_up_photos().size(); i++ ){
				imagePathMap.put( i+1, meetUp.getSports_meet_up_photos().get( i ).getImage_name()); //putting web url
				for(int j = i+1; j<=imagePathMap.size();j++) {
					if ( imagePathMap.get( j ) != null ) {
						context.oBaseApp.doLog( "j" + j + " path : " + imagePathMap.get( j ));
						switch ( j ) {
							case 1:
								Picasso.with( context ).load( imagePathMap.get( j )).into( imgPic1 );
								imgPic1.setTag( meetUp.getSports_meet_up_photos().get( i ).getId() );
								imgPicDelete1.setVisibility( View.VISIBLE );
								break;
							case 2:
								Picasso.with( context ).load( imagePathMap.get( j )).into( imgPic2 );
								imgPic2.setTag( meetUp.getSports_meet_up_photos().get( i ).getId() );
								imgPicDelete2.setVisibility( View.VISIBLE );
								break;
							case 3:
								Picasso.with( context ).load( imagePathMap.get( j )).into( imgPic3 );
								imgPic3.setTag( meetUp.getSports_meet_up_photos().get( i ).getId() );
								imgPicDelete3.setVisibility( View.VISIBLE );
								break;
							case 4:
								Picasso.with( context ).load( imagePathMap.get( j )).into( imgPic4 );
								imgPic4.setTag( meetUp.getSports_meet_up_photos().get( i ).getId() );
								imgPicDelete4.setVisibility( View.VISIBLE );
								break;
							case 5:
								Picasso.with( context ).load( imagePathMap.get( j )).into( imgPic5 );
								imgPic5.setTag( meetUp.getSports_meet_up_photos().get( i ).getId() );
								imgPicDelete5.setVisibility( View.VISIBLE );
								break;
						}
					}
					break;
				}
			}
		}
	}

	private void initValues() {
		btnCreate.setOnClickListener(this);
		edtTime.setOnClickListener(this);
		edtDate.setOnClickListener(this);
		edtAgeLimit.setOnClickListener(this);

		imgPic1.setOnClickListener(this);
		imgPic2.setOnClickListener(this);
		imgPic3.setOnClickListener(this);
		imgPic4.setOnClickListener(this);
		imgPic5.setOnClickListener(this);

		View.OnClickListener picDeleteListener = new View.OnClickListener() {
			@Override
			public void onClick( View view ) {
				switch ( view.getId() ){
					case R.id.CreateMeetupFragment_IMG_pic_delete1:
						if(imgPic1.getTag().toString().contains( "/" )){ // create
							imgPic1.setImageDrawable(null);
							imagePathMap.remove( 1 );
							imgPicDelete1.setVisibility( View.INVISIBLE );
						}else{//edit
							if(imgPic1.getDrawable() != null)
								callDeleteMeetupPhotoPopup( 1 , ( int ) imgPic1.getTag() );
						}
						break;
					case R.id.CreateMeetupFragment_IMG_pic_delete2:
						if(imgPic2.getTag().toString().contains( "/" )){ // create
							imgPic2.setImageDrawable(null);
							imagePathMap.remove( 2 );
							imgPicDelete2.setVisibility( View.INVISIBLE );
						}else{//edit
							if(imgPic2.getDrawable() != null)
								callDeleteMeetupPhotoPopup( 2, ( int ) imgPic2.getTag() );
						}
						break;
					case R.id.CreateMeetupFragment_IMG_pic_delete3:
						if(imgPic3.getTag().toString().contains( "/" )){ // create
							imgPic3.setImageDrawable(null);
							imagePathMap.remove( 3 );
							imgPicDelete3.setVisibility( View.INVISIBLE );
						}else{//edit
							if(imgPic3.getDrawable() != null)
								callDeleteMeetupPhotoPopup( 3, ( int ) imgPic3.getTag() );
						}
						break;
					case R.id.CreateMeetupFragment_IMG_pic_delete4:
						if(imgPic4.getTag().toString().contains( "/" )){ // create
							imgPic4.setImageDrawable(null);
							imagePathMap.remove( 4 );
							imgPicDelete4.setVisibility( View.INVISIBLE );
						}else{//edit
							if(imgPic4.getDrawable() != null)
								callDeleteMeetupPhotoPopup( 4, ( int ) imgPic4.getTag() );
						}
						break;
					case R.id.CreateMeetupFragment_IMG_pic_delete5:
						if(imgPic5.getTag().toString().contains( "/" )){ // create
							imgPic5.setImageDrawable(null);
							imagePathMap.remove( 5 );
							imgPicDelete5.setVisibility( View.INVISIBLE );
						}else{//edit
							if(imgPic5.getDrawable() != null)
								callDeleteMeetupPhotoPopup( 5, ( int ) imgPic5.getTag() );
						}
						break;
				}
			}
		};

		imgPicDelete1.setOnClickListener( picDeleteListener );
		imgPicDelete2.setOnClickListener( picDeleteListener );
		imgPicDelete3.setOnClickListener( picDeleteListener );
		imgPicDelete4.setOnClickListener( picDeleteListener );
		imgPicDelete5.setOnClickListener( picDeleteListener );
	}

	private void callDeleteMeetupPhotoPopup( final int pos, final int imageId ) {
		new AlertDialog.Builder( context)
				.setMessage(R.string.msg_delete_pic)
				.setCancelable( false )
				.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						callDeleteMeetupPhotoService( pos, imageId );
					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				})
				.show();
	}

	private void callDeleteMeetupPhotoService( final int pos, int imageId ) {
		context.showProgress();

		HashMap<String, String> mapFields = new HashMap<>();
		mapFields.put("sports_meet_up_photo_id",imageId+"");
		mapFields.put("sports_meet_up_detail_id",meetUp.getId()+"");
		context.oBaseApp.doLog("FieldMap: "+ mapFields);

		RetroInterface              apiService = context.oBaseApp.getClient().create(RetroInterface.class);
		Call<StatusObject > call       = apiService.postDeleteSportMeetUpPhoto( SharedPreferenceHelper.getAuthToken( context), mapFields);
		call.enqueue(new RetroCallback<StatusObject>(context) {
			@Override
			public void onSuccess(Call<StatusObject> call, Response<StatusObject> response) {
				context.oBaseApp.doToast(response.body().getUser_msg());
				switch ( pos ){
					case 1:
						imgPic1.setImageDrawable(null);
						imagePathMap.remove( 1 );
						imgPicDelete1.setVisibility( View.INVISIBLE );
						break;
					case 2:
						imgPic2.setImageDrawable(null);
						imagePathMap.remove( 2 );
						imgPicDelete2.setVisibility( View.INVISIBLE );
						break;
					case 3:
						imgPic3.setImageDrawable(null);
						imagePathMap.remove( 3 );
						imgPicDelete3.setVisibility( View.INVISIBLE );
						break;
					case 4:
						imgPic4.setImageDrawable(null);
						imagePathMap.remove( 4 );
						imgPicDelete4.setVisibility( View.INVISIBLE );
						break;
					case 5:
						imgPic5.setImageDrawable(null);
						imagePathMap.remove( 5 );
						imgPicDelete5.setVisibility( View.INVISIBLE );
						break;
				}
			}
		});
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(true);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
		super.onCreateOptionsMenu(menu, inflater);
		menu.clear();
	}

	@Override
	public void onResume() {
		super.onResume();
		context.setToolbarTitle(getString(R.string.createSportMeetup));
		context.setHomeButtonDisabled();
		context.setselectedTab(2); // position of contact us
	}

	private void callEditMeetupService() {
		context.showProgress();
		RetroInterface apiService = context.oBaseApp.getClient().create(RetroInterface.class);

		HashMap<String, RequestBody> mapFields = new HashMap<>();
		mapFields.put("id",Util.createPartFromString( meetUp.getId()+""));
		mapFields.put("title",Util.createPartFromString( edtTitle.getText().toString().trim()));
		mapFields.put("meetup_datetime", Util.createPartFromString( edtDate.getText().toString().trim() + " "+ edtTime.getText().toString().trim() +":00"));
		mapFields.put("description", Util.createPartFromString( edtDescription.getText().toString().trim()));
		mapFields.put("age_limit_id",Util.createPartFromString( Util.getAgeGroupKey( edtAgeLimit.getText().toString().trim() )+"")); // getting tag as we need to send id of agegroup
		mapFields.put("total_allowed_members", Util.createPartFromString( edtAllowedMembers.getText().toString().trim()));
		mapFields.put("minimum_required_members", Util.createPartFromString( edtMinNoOfMembers.getText().toString().trim()));

		String selection = null;
		if(radioGroup.getCheckedRadioButtonId()!=-1){
			int id= radioGroup.getCheckedRadioButtonId();
			View radioButton = radioGroup.findViewById(id);
			int radioId = radioGroup.indexOfChild(radioButton);
			RadioButton btn = (RadioButton) radioGroup.getChildAt(radioId);
			selection = (String) btn.getText();
		}
		mapFields.put("meetup_type", Util.createPartFromString(selection.toLowerCase()));

		imageArrayList =new ArrayList<>(  );
		for ( int i = 1; i<=5;i++ ){
			if( imagePathMap.get(i) !=null) {
				context.oBaseApp.doLog( "Map : " + i + "  : " + imagePathMap.get( i ) );
				if (!imagePathMap.get(i).contains( "http://" ) ) {
					File file = new File( imagePathMap.get( i ) );

					MultipartBody.Part body;
					RequestBody        requestFile = null;
					if ( file != null ) {
						requestFile = RequestBody.create( MediaType.parse( "image/png" ), file );
						context.oBaseApp.doLog( "request" + requestFile.toString() );
						body = MultipartBody.Part.createFormData( "sports_meet_up_pic_" + i, file.getName(), requestFile );
					}
					else {
						body = null;
					}
					imageArrayList.add( body );

				}
			}

		}

		context.oBaseApp.doLog("FieldMap: "+ mapFields);

		Call<StatusObject> call = apiService.postUpdateMeetUp(SharedPreferenceHelper.getAuthToken(context),mapFields,imageArrayList);
		call.enqueue(new RetroCallback<StatusObject>(context) {
			@Override
			public void onSuccess(Call<StatusObject> call, Response<StatusObject> response) {
				context.oBaseApp.doToast(response.body().getUser_msg());
				clearViewValues();
				Util.closeEditor(context);
				context.setselectedTab(3); // to set tab indicator to My Meetups
				context.doStackFragment(new MyMeetUpsFragment(), context.frameLayoutContainer, "");
			}

			@Override
			public void onError( Call< StatusObject > call, Response< StatusObject > response ) {
				super.onError( call, response );
				context.oBaseApp.doToast(" Error: "+response.body().getUser_msg());
			}
		});
	}

	private void callCreateMeetupService() {
		context.showProgress();
		RetroInterface apiService = context.oBaseApp.getClient().create(RetroInterface.class);

		HashMap<String, RequestBody> mapFields = new HashMap<>();
		mapFields.put("title",Util.createPartFromString( edtTitle.getText().toString().trim()));
		mapFields.put("meetup_datetime", Util.createPartFromString( edtDate.getText().toString().trim() + " "+ edtTime.getText().toString().trim() +":00"));
		mapFields.put("description", Util.createPartFromString( edtDescription.getText().toString().trim()));
		mapFields.put("age_limit_id",Util.createPartFromString(Util.getAgeGroupKey( edtAgeLimit.getText().toString().trim())+"")); // getting tag as we need to send id of agegroup
		mapFields.put("total_allowed_members", Util.createPartFromString( edtAllowedMembers.getText().toString().trim()));
		mapFields.put("minimum_required_members", Util.createPartFromString( edtMinNoOfMembers.getText().toString().trim()));

		String selection = null;
		if(radioGroup.getCheckedRadioButtonId()!=-1){
			int id= radioGroup.getCheckedRadioButtonId();
			View radioButton = radioGroup.findViewById(id);
			int radioId = radioGroup.indexOfChild(radioButton);
			RadioButton btn = (RadioButton) radioGroup.getChildAt(radioId);
			selection = (String) btn.getText();
		}
		mapFields.put("meetup_type", Util.createPartFromString(selection.toLowerCase()));

		imageArrayList =new ArrayList<>(  );
		for ( int i = 1; i<=5;i++ ){
			if( imagePathMap.get(i) !=null) {
				context.oBaseApp.doLog( "Map : " + i + "  : " + imagePathMap.get( i ) );
				if (!imagePathMap.get(i).contains( "http://" ) ) {
					File file = new File( imagePathMap.get( i ) );

					MultipartBody.Part body;
					RequestBody        requestFile = null;
					if ( file != null ) {
						requestFile = RequestBody.create( MediaType.parse( "image/png" ), file );
						context.oBaseApp.doLog( "request" + requestFile.toString() );
						body = MultipartBody.Part.createFormData( "sports_meet_up_pic_" + i, file.getName(), requestFile );
					}
					else {
						body = null;
					}
					imageArrayList.add( body );

				}
			}

		}

		context.oBaseApp.doLog("FieldMap: "+ mapFields);

		Call<StatusObject> call = apiService.postCreateMeetUp(SharedPreferenceHelper.getAuthToken(context),mapFields,imageArrayList);
		call.enqueue(new RetroCallback<StatusObject>(context) {
			@Override
			public void onSuccess(Call<StatusObject> call, Response<StatusObject> response) {
				context.oBaseApp.doToast(response.body().getUser_msg());
				clearViewValues();
				Util.closeEditor(context);
				context.setselectedTab(3); // to set tab indicator to My Meetups
				context.doStackFragment(new MyMeetUpsFragment(), context.frameLayoutContainer, "");
			}

			@Override
			public void onError( Call< StatusObject > call, Response< StatusObject > response ) {
				super.onError( call, response );
				context.oBaseApp.doToast(" Error: "+response.body().getUser_msg());
			}
		});
	}

	private void clearViewValues() {
		edtTitle.setText("");
		edtDate.setText("");
		edtTime.setText("");
		edtDescription.setText("");
		edtAgeLimit.setText("");
		edtAllowedMembers.setText("");
		edtMinNoOfMembers.setText( "" );
	}

	private void showTimePicker(){
		Calendar mcurrentTime = Calendar.getInstance();
		int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
		int minute = mcurrentTime.get(Calendar.MINUTE);
		TimePickerDialog mTimePicker;
		mTimePicker = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
			@Override
			public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
				edtTime.setText( String.format("%02d", selectedHour) + ":" + String.format("%02d", selectedMinute));
			}
		}, hour, minute, true);//Yes 24 hour time
		mTimePicker.setTitle("Select Time");
		mTimePicker.show();
	}

	private void showDatePicker() {
		Calendar newCalendar = Calendar.getInstance();
		datePickerDialog = new DatePickerDialog( context, new DatePickerDialog.OnDateSetListener() {

			public void onDateSet( DatePicker view, int year, int monthOfYear, int dayOfMonth ) {
				Calendar newDate = Calendar.getInstance();
				newDate.set( year, monthOfYear, dayOfMonth );
				edtDate.setText( new SimpleDateFormat( "yyyy-MM-dd" ).format( newDate.getTime() ) );
			}

		}, newCalendar.get( Calendar.YEAR ), newCalendar.get( Calendar.MONTH ), newCalendar.get( Calendar.DAY_OF_MONTH ) );
		datePickerDialog.show();
	}

	@Override
	public void onClick( View view ) {
		Util.closeEditor( context );
		switch ( view.getId() ) {
			case R.id.CreateMeetupFragment_BTN_create:
				vaildateInputs();
				break;
			case R.id.CreateMeetupFragment_ET_date:
				showDatePicker();
				break;
			case R.id.CreateMeetupFragment_ET_time:
				showTimePicker();
				break;
			case R.id.CreateMeetupFragment_ET_age_group:
				setAgeGroup();
				break;
			case R.id.CreateMeetupFragment_IMG_pic1:
				imgPos = 1;
				CreateMeetupFragmentPermissionsDispatcher.showGalleryWithCheck( this );
				break;
			case R.id.CreateMeetupFragment_IMG_pic2:
				imgPos = 2;
				CreateMeetupFragmentPermissionsDispatcher.showGalleryWithCheck( this );
				break;
			case R.id.CreateMeetupFragment_IMG_pic3:
				imgPos = 3;
				CreateMeetupFragmentPermissionsDispatcher.showGalleryWithCheck( this );
				break;
			case R.id.CreateMeetupFragment_IMG_pic4:
				imgPos = 4;
				CreateMeetupFragmentPermissionsDispatcher.showGalleryWithCheck( this );
				break;
			case R.id.CreateMeetupFragment_IMG_pic5:
				imgPos = 5;
				CreateMeetupFragmentPermissionsDispatcher.showGalleryWithCheck( this );
				break;
		}
	}

	private void vaildateInputs() {
		if( NetworkUtil.isConnected( context)){
			if(edtTitle.getText().toString().trim().length()>0){
				if(edtDate.getText().toString().trim().length()>0){
					if(edtTime.getText().toString().trim().length()>0){
						if(edtDescription.getText().toString().trim().length()>0){
							if(edtAgeLimit.getText().toString().trim().length()>0){
								if(edtAllowedMembers.getText().toString().trim().length()>0){
									if(Integer.parseInt( edtAllowedMembers.getText().toString() ) >= 2){
										if(edtMinNoOfMembers.getText().toString().trim().length()>0){
											if(Integer.parseInt( edtMinNoOfMembers.getText().toString() ) >= 2){
												if(btnCreate.getText().toString().equalsIgnoreCase( "Update" )){
													callEditMeetupService();
												}else{
													callCreateMeetupService();
												}
											}else{
												context.oBaseApp.doToast("Minimum no of members should be at least 2.");
											}
										}else{
											context.oBaseApp.doToast("Please enter Minimum no of members.");
										}
									}else{
										context.oBaseApp.doToast("Total allowed members should be at least 2.");
									}
								}else{
									context.oBaseApp.doToast("Please enter Total allowed members.");
								}
							}else{
								context.oBaseApp.doToast("Please select Age Limit for MeetUp.");
							}
						}else{
							context.oBaseApp.doToast("Please enter Description.");
						}
					}else{
						context.oBaseApp.doToast("Please select time of MeetUp.");
					}
				}else{
					context.oBaseApp.doToast("Please select date of MeetUp.");
				}
			}else{
				context.oBaseApp.doToast("Please enter valid MeetUp Title.");
			}
		}else{
			context.oBaseApp.doToast(getString(R.string.msg_noInternet));
		}
	}

	public void setAgeGroup() {
		final CharSequence[] ageGroup = { "0-18", "19-35", "36-60", "61-80", "80-100" };
		AlertDialog.Builder  alert    = new AlertDialog.Builder( context );
		alert.setTitle( "Select Age group" );
		alert.setSingleChoiceItems( ageGroup, -1, new DialogInterface.OnClickListener() {
			@Override
			public void onClick( DialogInterface dialog, int which ) {
				edtAgeLimit.setText( ageGroup[which] + "" );
				//edtAgeLimit.setTag( which + 1 );// setting tag as we need to send id of agegroup starting from 1-5
				dialog.dismiss();
			}
		} );
		alert.show();
	}

	@Override
	public void onActivityResult( int requestCode, int resultCode, Intent data ) {
		super.onActivityResult( requestCode, resultCode, data );
		if(requestCode == REQUEST_SELECT_PHOTO_GALLERY && resultCode == RESULT_OK){
			ArrayList<Image > images = data.getParcelableArrayListExtra( Constants.INTENT_EXTRA_IMAGES);
			for(int i = 0; i<images.size();i++){
				File file = new File( images.get( i ).path );
				try {
					InputStream ims = new FileInputStream( file );
					mCurrentPhotoPath = images.get( i ).path;

					for(int j = 1; j<=5;j++){
						if(imagePathMap.get( j ) == null){
							imagePathMap.put( j, mCurrentPhotoPath ); // putting image path from 1-5
							context.oBaseApp.doLog( "j" + j + " path : " + mCurrentPhotoPath);
							switch ( j ){
								case 1:
									imgPic1.setImageBitmap( BitmapFactory.decodeStream( ims ) );
									imgPic1.setTag( mCurrentPhotoPath );
									imgPicDelete1.setVisibility( View.VISIBLE );
									break;
								case 2:
									imgPic2.setImageBitmap( BitmapFactory.decodeStream( ims ) );
									imgPic2.setTag( mCurrentPhotoPath );
									imgPicDelete2.setVisibility( View.VISIBLE );
									break;
								case 3:
									imgPic3.setImageBitmap( BitmapFactory.decodeStream( ims ) );
									imgPic3.setTag( mCurrentPhotoPath );
									imgPicDelete3.setVisibility( View.VISIBLE );
									break;
								case 4:
									imgPic4.setImageBitmap( BitmapFactory.decodeStream( ims ) );
									imgPic4.setTag( mCurrentPhotoPath );
									imgPicDelete4.setVisibility( View.VISIBLE );
									break;
								case 5:
									imgPic5.setImageBitmap( BitmapFactory.decodeStream( ims ) );
									imgPic5.setTag( mCurrentPhotoPath );
									imgPicDelete5.setVisibility( View.VISIBLE );
									break;
							}
							break;
						}
					}
				}
				catch ( FileNotFoundException e ) {
					return;
				}
			}
		}
	}

	@Override
	public void onRequestPermissionsResult( int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults ) {
		super.onRequestPermissionsResult( requestCode, permissions, grantResults );
		CreateMeetupFragmentPermissionsDispatcher.onRequestPermissionsResult( this, requestCode, grantResults );
	}

	private void galleryIntent(){
		Intent intent = new Intent(context, AlbumSelectActivity.class);
		//set limit on number of images that can be selected, default is 10
		intent.putExtra( Constants.INTENT_EXTRA_LIMIT, 5-imagePathMap.size());
		startActivityForResult(intent, REQUEST_SELECT_PHOTO_GALLERY);
	}

	@NeedsPermission( Manifest.permission.WRITE_EXTERNAL_STORAGE )
	void showGallery() {
		galleryIntent();
	}

	@OnShowRationale( Manifest.permission.WRITE_EXTERNAL_STORAGE )
	void showRationaleForCamera( final PermissionRequest request ) {
		new AlertDialog.Builder( context )
				.setMessage( "Access to External Storage is required" )
				.setPositiveButton( "Allow", new DialogInterface.OnClickListener() {
					@Override
					public void onClick( DialogInterface dialogInterface, int i ) {
						request.proceed();
					}
				} )
				.setNegativeButton( "Deny", new DialogInterface.OnClickListener() {
					@Override
					public void onClick( DialogInterface dialogInterface, int i ) {
						request.cancel();
					}
				} )
				.show();
	}

	@OnPermissionDenied( Manifest.permission.WRITE_EXTERNAL_STORAGE )
	void onCameraDenied() {
		Toast.makeText( context, "denied", Toast.LENGTH_SHORT ).show();
	}

	@OnNeverAskAgain( Manifest.permission.WRITE_EXTERNAL_STORAGE )
	void onCameraNeverAskAgain() {
		Toast.makeText( context, "Allow All permissions.", Toast.LENGTH_SHORT ).show();
		Util.openPermissionSettings( context );
	}
}
