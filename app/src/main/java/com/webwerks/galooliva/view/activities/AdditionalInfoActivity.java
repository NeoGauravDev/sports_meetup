package com.webwerks.galooliva.view.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.squareup.picasso.Picasso;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.AdditionalInfo;
import com.webwerks.galooliva.data.network.model.AdditionalInfoResponse;
import com.webwerks.galooliva.data.network.model.LoginResponse;
import com.webwerks.galooliva.data.network.model.StatusObject;
import com.webwerks.galooliva.data.network.model.User;
import com.webwerks.galooliva.utils.Util;
import com.webwerks.galooliva.view.fragments.BottomSheetImageDialogFragment;
import com.webwerks.galooliva.view.fragments.MyMeetUpsFragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Response;

@RuntimePermissions
public class AdditionalInfoActivity extends BaseActivity implements View.OnClickListener {

	private static final int REQUEST_SELECT_PHOTO_GALLERY = 1003;
	private AdditionalInfoActivity context;
	private TextView txtFullName, txtCityCountry, txtHeight, txtWeight, txtDominantHand, txtBirthPlace,
			txtDob, txtMyInterest, txtGoals;
	private ImageView imgProfilePic, imgEditPic, imgEditInfo , imgBack;
	BottomSheetDialogFragment bottomSheetDialogFragment;
	AdditionalInfo additionalInfo;

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_additional_info );
		init();
	}
	private void init() {
		context = this;
		initViews();
		initValues();
	}

	private void initViews() {
		imgProfilePic = (ImageView) findViewById( R.id.AdditionalInfoActivity_IMG_profile_pic );
		imgEditPic = (ImageView) findViewById( R.id.AdditionalInfoActivity_IMG_edit_pic );
		imgEditInfo= (ImageView) findViewById( R.id.AdditionalInfoActivity_IMG_edit_info );
		imgBack= (ImageView) findViewById( R.id.AdditionalInfoActivity_IMG_back );

		txtFullName = (TextView ) findViewById( R.id.AdditionalInfoActivity_TV_full_name );
		txtCityCountry = (TextView ) findViewById( R.id.AdditionalInfoActivity_TV_city_country );
		txtHeight = (TextView ) findViewById( R.id.AdditionalInfoActivity_TV_height );
		txtWeight = (TextView ) findViewById( R.id.AdditionalInfoActivity_TV_weight );
		txtDominantHand = (TextView ) findViewById( R.id.AdditionalInfoActivity_TV_dominant_hand );
		txtBirthPlace = (TextView ) findViewById( R.id.AdditionalInfoActivity_TV_birthplace );
		txtDob = (TextView ) findViewById( R.id.AdditionalInfoActivity_TV_dob );
		txtMyInterest = (TextView ) findViewById( R.id.AdditionalInfoActivity_TV_interst );
		txtGoals = (TextView ) findViewById( R.id.AdditionalInfoActivity_TV_goals );

		bottomSheetDialogFragment = new BottomSheetImageDialogFragment();
	}

	private void initValues() {
		imgEditPic.setOnClickListener( this );
		imgEditInfo.setOnClickListener( this );
		imgBack.setOnClickListener( this );
	}

	@Override
	protected void onResume() {
		super.onResume();
		callGetUserAdditionalInfoService();
	}

	@Override
	public void onClick( View view ) {
		switch ( view.getId() ){
			case R.id.AdditionalInfoActivity_IMG_edit_pic:
				//showBottomSheetImagePick();
				AdditionalInfoActivityPermissionsDispatcher.showGalleryWithCheck( this );
				break;
			case R.id.AdditionalInfoActivity_IMG_edit_info:
				Intent intent =new Intent( context,AdditionalInfoEditActivity.class );
				intent.putExtra( "additionalInfo", additionalInfo );
				startActivity( intent );
				break;
			case R.id.AdditionalInfoActivity_IMG_back:
				finish();
				break;
		}
	}

	private void galleryIntent(){
		Intent intent = new Intent(context, AlbumSelectActivity.class);
		//set limit on number of images that can be selected, default is 10
		intent.putExtra( Constants.INTENT_EXTRA_LIMIT, 1);
		startActivityForResult(intent, REQUEST_SELECT_PHOTO_GALLERY);
	}

	@NeedsPermission( Manifest.permission.WRITE_EXTERNAL_STORAGE )
	void showGallery() {
		galleryIntent();
	}

	@OnShowRationale( Manifest.permission.WRITE_EXTERNAL_STORAGE )
	void showRationaleForCamera( final PermissionRequest request ) {
		new AlertDialog.Builder( context )
				.setMessage( "Access to External Storage is required" )
				.setPositiveButton( "Allow", new DialogInterface.OnClickListener() {
					@Override
					public void onClick( DialogInterface dialogInterface, int i ) {
						request.proceed();
					}
				} )
				.setNegativeButton( "Deny", new DialogInterface.OnClickListener() {
					@Override
					public void onClick( DialogInterface dialogInterface, int i ) {
						request.cancel();
					}
				} )
				.show();
	}

	@OnPermissionDenied( Manifest.permission.WRITE_EXTERNAL_STORAGE )
	void onCameraDenied() {
		Toast.makeText( context, "denied", Toast.LENGTH_SHORT ).show();
	}

	@OnNeverAskAgain( Manifest.permission.WRITE_EXTERNAL_STORAGE )
	void onCameraNeverAskAgain() {
		Toast.makeText( context, "Allow All permissions.", Toast.LENGTH_SHORT ).show();
		Util.openPermissionSettings( context );
	}

	@Override
	public void onActivityResult( int requestCode, int resultCode, Intent data ) {
		super.onActivityResult( requestCode, resultCode, data );
		if(requestCode == REQUEST_SELECT_PHOTO_GALLERY && resultCode == RESULT_OK){
			ArrayList<Image > images = data.getParcelableArrayListExtra( Constants.INTENT_EXTRA_IMAGES);
			callUpdateProfilePicService( images.get( 0 ).path );
		}
	}

	private void showBottomSheetImagePick() {
		bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
	}

	private void callGetUserAdditionalInfoService() {
		showProgress();
		RetroInterface apiService = oBaseApp.getClient().create( RetroInterface.class);

		Call<AdditionalInfoResponse> call = apiService.getUserAdditionalInfo( SharedPreferenceHelper.getAuthToken( context));
		call.enqueue(new RetroCallback<AdditionalInfoResponse>( context) {
			@Override
			public void onSuccess(Call<AdditionalInfoResponse> call, Response<AdditionalInfoResponse> response) {
				additionalInfo = response.body().getData();
				setValues(additionalInfo);
			}
		});
	}

	private void setValues( AdditionalInfo additionalInfo ) {
		txtFullName.setText(additionalInfo.getUser().getFull_name() );
		txtCityCountry.setText( additionalInfo.getUser().getCity()+", " + additionalInfo.getUser().getCountry());
		txtHeight.setText(additionalInfo.getHeight()+"");
		txtWeight.setText(additionalInfo.getWeight()+"");
		txtDominantHand.setText(additionalInfo.getDominant_hand());
		txtBirthPlace.setText(additionalInfo.getBirth_place());
		txtDob.setText(additionalInfo.getUser().getDob());
		txtMyInterest.setText(additionalInfo.getMy_interest());
		txtGoals.setText(additionalInfo.getGoals_or_synonyms());

		Picasso.with( context ).load( additionalInfo.getUser().getProfile_pic()).into( imgProfilePic );
	}


	private void callUpdateProfilePicService( final String path) {
		context.showProgress();
		RetroInterface apiService = context.oBaseApp.getClient().create(RetroInterface.class);

		File file = new File(path);
		MultipartBody.Part body;
		RequestBody        requestFile = null;
		if ( file != null ) {
			requestFile = RequestBody.create( MediaType.parse( "image/png" ), file );
			context.oBaseApp.doLog( "request" + requestFile.toString() );
			body = MultipartBody.Part.createFormData( "profile_pic" , file.getName(), requestFile );
		}
		else {
			body = null;
		}

		Call<StatusObject > call = apiService.postChangeProfilePicture( SharedPreferenceHelper.getAuthToken( context), body);
		call.enqueue(new RetroCallback<StatusObject>(context) {
			@Override
			public void onSuccess(Call<StatusObject> call, Response<StatusObject> response) {
				context.oBaseApp.doToast(response.body().getUser_msg());
				File file = new File(path);
				try {
					InputStream ims = new FileInputStream( file );
					imgProfilePic.setImageBitmap( BitmapFactory.decodeStream( ims ) );
				}
				catch ( FileNotFoundException e ) {
					e.printStackTrace();
				}
			}
		});
	}
}
