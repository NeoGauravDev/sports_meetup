package com.webwerks.galooliva.view.fragments;

import android.Manifest;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.Contact;
import com.webwerks.galooliva.data.network.model.StatusObject;
import com.webwerks.galooliva.utils.Util;
import com.webwerks.galooliva.view.activities.HomeActivity;
import com.webwerks.galooliva.view.adapters.ContactRVAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.OnNeverAskAgain;
import permissions.dispatcher.OnPermissionDenied;
import permissions.dispatcher.OnShowRationale;
import permissions.dispatcher.PermissionRequest;
import permissions.dispatcher.RuntimePermissions;
import retrofit2.Call;
import retrofit2.Response;

@RuntimePermissions
public class ContactUsFragment extends Fragment {

    private HomeActivity context; // context variable represents Activity scope
    private View rootView;

	private ContactRVAdapter contactRVAdapter;
	private   RecyclerView   recyclerView;
	private   EditText       edtSearch;
	ContentResolver resolver;
	Cursor          phones, email;
	private ArrayList<Contact> contacts = new ArrayList<>(  );

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_meet_up_list, container,false);
        init();
        return rootView;
    }

    private void init() {
        context = (HomeActivity) getActivity();
        initViews();
        initValues();
    }

    private void initViews() {
	    recyclerView = (RecyclerView) rootView.findViewById(R.id.MeetUpListFragment_RV_meetup );
	    edtSearch = (EditText) rootView.findViewById( R.id.MeetUpListFragment_ET_search );
    }

    private void initValues() {
	    ContactUsFragmentPermissionsDispatcher.showContactsWithCheck( this );
	    edtSearch.addTextChangedListener(new TextWatcher() {

		    public void afterTextChanged(Editable s) {}

		    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

		    public void onTextChanged(CharSequence query, int start, int before, int count) {

			    query = query.toString().toLowerCase();

			    final ArrayList<Contact> filteredList = new ArrayList<>();
			    if( contacts != null) {
				    for ( int i = 0; i < contacts.size(); i++ ) {
					    final String text = contacts.get( i ).getName().toLowerCase();
					    if ( text.startsWith( query + "" ) ) {
						    filteredList.add( contacts.get( i ) );
					    }
				    }
			    }
			    contactRVAdapter = new ContactRVAdapter( context, filteredList);
			    recyclerView.setAdapter( contactRVAdapter );
			    contactRVAdapter.notifyDataSetChanged();
		    }
	    });
    }

    @Override
    public void onResume() {
        super.onResume();
        context.setToolbarTitle(getString(R.string.contact ));
        context.setHomeButtonDisabled();
        context.setselectedTab(0); // position of contacts us
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    public void callContactUsService(){
        context.showProgress();

        Map<String, String> mapFields = new HashMap<>();
        context.oBaseApp.doLog("FieldMap: "+ mapFields);

        RetroInterface apiService = context.oBaseApp.getClient().create(RetroInterface.class);
        Call<StatusObject> call = apiService.postContactUs(SharedPreferenceHelper.getAuthToken(context),mapFields);
        call.enqueue(new RetroCallback<StatusObject>(context) {
            @Override
            public void onSuccess(Call<StatusObject> call, Response<StatusObject> response) {
                try {
                    StatusObject statusObject = response.body();
                    context.oBaseApp.doToast(statusObject.getUser_msg());
                    context.finish();
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

	class LoadContact extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected Void doInBackground(Void... voids) {
			// Get Contact list from Phone

			if (phones != null) {
				Log.e( "count", "" + phones.getCount());
				if (phones.getCount() == 0) {
					context.runOnUiThread( new Runnable() {
						@Override
						public void run() {
							Toast.makeText(context, "No contacts in your contacts list.", Toast.LENGTH_LONG).show();
						}
					} );
				}

				while (phones.moveToNext()) {
					Bitmap bit_thumb   = null;
					String id          = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
					String name        = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
					String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					String EmailAddr   = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA2));
					String image_thumb = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
					try {
						if (image_thumb != null) {
							bit_thumb = MediaStore.Images.Media.getBitmap( resolver, Uri.parse( image_thumb));
						} else {
							Log.e("No Image Thumb", "--------------");
						}
					} catch (IOException e) {
						e.printStackTrace();
					}

					Contact selectUser = new Contact();
					selectUser.setThumb(bit_thumb);
					selectUser.setName(name);
					selectUser.setPhone(phoneNumber);
					selectUser.setEmail(id);
					contacts.add(selectUser);
				}
			} else {
				Log.e("Cursor close 1", "----------------");
			}
			//phones.close();
			return null;
		}

		@Override
		protected void onPostExecute(Void aVoid) {
			super.onPostExecute(aVoid);
			contactRVAdapter = new ContactRVAdapter( context, contacts);
			RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager( context);
			recyclerView.setLayoutManager(mLayoutManager);
			recyclerView.setItemAnimator(new DefaultItemAnimator());
			recyclerView.setAdapter(contactRVAdapter);
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		if(phones != null){
			phones.close();
		}
	}

	@Override
	public void onRequestPermissionsResult( int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults ) {
		super.onRequestPermissionsResult( requestCode, permissions, grantResults );
		ContactUsFragmentPermissionsDispatcher.onRequestPermissionsResult( this, requestCode, grantResults );
	}


	@NeedsPermission( Manifest.permission.READ_CONTACTS )
	void showContacts() {
		resolver = context.getContentResolver();
		phones = context.getContentResolver().query( ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC");
		LoadContact loadContact = new LoadContact();
		loadContact.execute();
	}

	@OnShowRationale( Manifest.permission.READ_CONTACTS )
	void showRationaleForCamera( final PermissionRequest request ) {
		new AlertDialog.Builder( context )
				.setMessage( "Access to Contacts is required" )
				.setPositiveButton( "Allow", new DialogInterface.OnClickListener() {
					@Override
					public void onClick( DialogInterface dialogInterface, int i ) {
						request.proceed();
					}
				} )
				.setNegativeButton( "Deny", new DialogInterface.OnClickListener() {
					@Override
					public void onClick( DialogInterface dialogInterface, int i ) {
						request.cancel();
					}
				} )
				.show();
	}


	@OnPermissionDenied( Manifest.permission.READ_CONTACTS )
	void onCameraDenied() {
		Toast.makeText( context, "denied", Toast.LENGTH_SHORT ).show();
	}

	@OnNeverAskAgain( Manifest.permission.READ_CONTACTS )
	void onCameraNeverAskAgain() {
		Toast.makeText( context, "Allow All permissions.", Toast.LENGTH_SHORT ).show();
		Util.openPermissionSettings( context );
	}
}
