package com.webwerks.galooliva.view.adapters;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.squareup.picasso.Picasso;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.network.model.MeetUp;
import com.webwerks.galooliva.utils.CircleTransform;
import com.webwerks.galooliva.view.activities.HomeActivity;
import com.webwerks.galooliva.view.fragments.CreateMeetupFragment;

import java.util.ArrayList;

/**
 * Created by webwerks on 19/12/16.
 */

public class MyMeetUpsRVAdapter extends RecyclerSwipeAdapter<MyMeetUpsRVAdapter.MyViewHolder> {

    private ArrayList<MeetUp> meetUpArrayList;
    private HomeActivity      context;


    public MyMeetUpsRVAdapter( HomeActivity context, ArrayList<MeetUp> meetUpArrayList){
        this.context =context;
        this.meetUpArrayList =meetUpArrayList;

    }

	public void removeItem(int position) {
		meetUpArrayList.remove(position);
		notifyItemRemoved(position);
		notifyItemRangeChanged(position, meetUpArrayList.size());
	}

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_my_meetups_rv_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder( MyViewHolder holder, final int position) {
	    holder.tvTitle.setText(meetUpArrayList.get(position).getTitle());
	    holder.tvDesc.setText(meetUpArrayList.get(position).getDescription());

	    if(meetUpArrayList.get(position).getSports_meet_up_photos().size()>0){
		   Picasso.with( context).load(meetUpArrayList.get( position).getSports_meet_up_photos().get( 0).getImage_name()).resize( 90, 90).centerCrop().into( holder.img);
	    }else{
		    Picasso.with( context).load( R.mipmap.ic_launcher).resize( 90, 90).centerCrop().into( holder.img);
	    }

	    holder.imgCreate.setOnClickListener( new View.OnClickListener() {
		    @Override
		    public void onClick( View view ) {
			    invitePeoplePopup();
		    }
	    } );

	    holder.imgEdit.setOnClickListener( new View.OnClickListener() {
		    @Override
		    public void onClick( View view ) {
			    CreateMeetupFragment createMeetupFragment =new CreateMeetupFragment();
			    Bundle bundle= new Bundle(  );
			    bundle.putSerializable( "meetup", meetUpArrayList.get( position ) );
			    createMeetupFragment.setArguments( bundle );
			    context.doStackFragment( createMeetupFragment,context.frameLayoutContainer, "");
			    //context.oBaseApp.doToast( "Edit pressed." );
		    }
	    } );

	    holder.imgDelete.setOnClickListener( new View.OnClickListener() {
		    @Override
		    public void onClick( View view ) {
			    context.oBaseApp.doToast( "Delete pressed." );
		    }
	    } );

    }

    @Override
    public int getItemCount() {
        return meetUpArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDesc;
        ImageView img, imgCreate, imgChat, imgEdit, imgDelete;
	    SwipeLayout swipeLayout;

        public MyViewHolder(View view) {
            super(view);
	        swipeLayout = (SwipeLayout ) view.findViewById( R.id.swipe);
            tvTitle = (TextView) view.findViewById( R.id.GroupsRVAdapter_TV_title);
            tvDesc = (TextView) view.findViewById( R.id.GroupsRVAdapter_TV_description);
            img = (ImageView) view.findViewById(R.id.GroupsRVAdapter_IMG);
            imgCreate = (ImageView) view.findViewById(R.id.GroupsRVAdapter_IMG_create );
            imgEdit = (ImageView) view.findViewById(R.id.GroupsRVAdapter_IMG_edit );
            imgDelete = (ImageView) view.findViewById(R.id.GroupsRVAdapter_IMG_delete );
        }
    }

	@Override
	public int getSwipeLayoutResourceId(int position) {
		return R.id.swipe;
	}

	private void invitePeoplePopup() {
		final Dialog dialog = new Dialog( context);
		dialog.requestWindowFeature( Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.popup_invite_people);
		Button dialogButton = (Button) dialog.findViewById( R.id.Popup_BTN_send);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}
}
