package com.webwerks.galooliva.view.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.webwerks.galooliva.R;

import javax.annotation.Nullable;

/**
 * Created by webwerks on 12/1/17.
 */

public class BottomSheetImageDialogFragment extends BottomSheetDialogFragment {

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

		@Override
		public void onStateChanged( @NonNull View bottomSheet, int newState) {
			if (newState == BottomSheetBehavior.STATE_HIDDEN) {
				dismiss();
			}
		}

		@Override
		public void onSlide(@NonNull View bottomSheet, float slideOffset) {
		}
	};

	@Override
	public void setupDialog( final Dialog dialog, int style) {
		super.setupDialog(dialog, style);
		View contentView = View.inflate(getContext(), R.layout.layout_bottom_sheet, null);
		dialog.setContentView(contentView);
		CoordinatorLayout.LayoutParams layoutParams =
				(CoordinatorLayout.LayoutParams) ((View) contentView.getParent()).getLayoutParams();
		CoordinatorLayout.Behavior behavior = layoutParams.getBehavior();
		if (behavior != null && behavior instanceof BottomSheetBehavior) {
			((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
		}

		TextView txtCamera = ( TextView ) contentView.findViewById( R.id.BottomSheet_TV_camera );
		TextView txtGallery = ( TextView ) contentView.findViewById( R.id.BottomSheet_TV_gallery );

		txtCamera.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick( View view ) {
				Toast.makeText( getActivity(), "Camera clicked.", Toast.LENGTH_SHORT ).show();
				dialog.dismiss();
			}
		} );

		txtGallery.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick( View view ) {
				Toast.makeText( getActivity(), "Gallery clicked.", Toast.LENGTH_SHORT ).show();
				dialog.dismiss();
			}
		} );

	}
}