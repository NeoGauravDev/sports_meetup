package com.webwerks.galooliva.view.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;

import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.MeetUpResponse;
import com.webwerks.galooliva.utils.NetworkUtil;
import com.webwerks.galooliva.view.activities.HomeActivity;
import com.webwerks.galooliva.view.adapters.MeetUpsRVAdapter;
import com.webwerks.galooliva.data.network.model.MeetUp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by webwerks on 21/12/16.
 */

public class MeetUpListFragment extends Fragment {

	private HomeActivity context; // context variable represents Activity scope
	private View rootView;

	private MeetUpsRVAdapter meetUpsRVAdapter;
	private RecyclerView recyclerView;
	private EditText edtSearch;
	private ImageView imgSearch;
	MeetUpResponse meetUpResponse = null;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.fragment_meet_up_list, container,false);
		setHasOptionsMenu(true);
		init();
		return rootView;
	}

	private void init() {
		context = (HomeActivity) getActivity();
		initViews();
		initValues();
	}

	private void initViews() {
		recyclerView = (RecyclerView) rootView.findViewById(R.id.MeetUpListFragment_RV_meetup );
		edtSearch = (EditText) rootView.findViewById( R.id.MeetUpListFragment_ET_search );
	}

	private void initValues() {
		Map<String, String> mapFields  = new HashMap<>();
		context.oBaseApp.doLog("FieldMap: "+ mapFields);
		callGetAllSportsMeetups( mapFields );
		edtSearch.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			public void onTextChanged(CharSequence query, int start, int before, int count) {

				query = query.toString().toLowerCase();

				final ArrayList<MeetUp> filteredList = new ArrayList<>();
				if(meetUpResponse != null) {
					for ( int i = 0; i < meetUpResponse.getData().size(); i++ ) {
						final String text = meetUpResponse.getData().get( i ).getTitle().toLowerCase();
						if ( text.startsWith( query + "" ) ) {
							filteredList.add( meetUpResponse.getData().get( i ) );
						}
					}
				}
				meetUpsRVAdapter = new MeetUpsRVAdapter(context,filteredList);
				recyclerView.setAdapter(meetUpsRVAdapter);
				meetUpsRVAdapter.notifyDataSetChanged();
			}
		});
	}

	private void callGetAllSportsMeetups( Map< String, String > mapFields ) {
		context.showProgress();
		RetroInterface          apiService = context.oBaseApp.getClient().create(RetroInterface.class);
		context.oBaseApp.doLog("Auth Token: "+ SharedPreferenceHelper.getAuthToken(context));
		Call<MeetUpResponse> call = apiService.getAllSportsMeetUpDetails(SharedPreferenceHelper.getAuthToken(context),mapFields);
		call.enqueue(new RetroCallback<MeetUpResponse>(context) {
			@Override
			public void onSuccess(Call<MeetUpResponse> call, Response<MeetUpResponse> response) {
				try{
					meetUpResponse = response.body();
					context.oBaseApp.doToast( meetUpResponse.getMessage() );
					if(meetUpResponse.getData() != null){
						meetUpsRVAdapter = new MeetUpsRVAdapter(context,meetUpResponse.getData());
						RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
						recyclerView.setLayoutManager(mLayoutManager);
						recyclerView.setItemAnimator(new DefaultItemAnimator());
						recyclerView.setAdapter(meetUpsRVAdapter);
					}

				}catch (Exception e){
					e.printStackTrace();
				}
			}
		});
	}


	@Override
	public void onResume() {
		super.onResume();
		context.setToolbarTitle(getString(R.string.meetUpList));
		context.setHomeButtonDisabled();
		context.unselectedTabs();
		FragmentManager fm = getActivity().getSupportFragmentManager();
		for(int i = 0; i < fm.getBackStackEntryCount(); ++i) {
			fm.popBackStack();
		}
	}

	@Override
	public void onCreateOptionsMenu( Menu menu, MenuInflater inflater) {
		// Do something that differs the Activity's menu here
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_filter:
				showPopupFilter();
				break;
		}

		return false;
	}

	private void showPopupFilter(){
		PopupMenu popup = new PopupMenu( context, context.findViewById( R.id.action_filter));
		popup.getMenuInflater().inflate(R.menu.popup_menu_filter, popup.getMenu());
		popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
			public boolean onMenuItemClick(MenuItem item) {
				switch ( item.getItemId() ){
					case R.id.action_all:
						initValues();
						break;
					case R.id.action_age:
						AgeGroupFilterPopup();
						break;
					case R.id.action_location:
						locationFilterPopup();
						break;
					case R.id.action_time:
						timeFilterPopup();
						break;
				}
				return true;
			}
		});
		popup.show();
	}

	public void AgeGroupFilterPopup() {
		final CharSequence[] ageGroup = { "0-18", "19-35", "36-60", "61-80", "80-100" };
		AlertDialog.Builder  alert    = new AlertDialog.Builder( context );
		alert.setTitle( "Select Age group" );
		alert.setSingleChoiceItems( ageGroup, -1, new DialogInterface.OnClickListener() {
			@Override
			public void onClick( DialogInterface dialog, int which ) {
				Map<String, String> mapFields  = new HashMap<>();
				mapFields.put( "age_limit_id", (which + 1)+"");
				context.oBaseApp.doLog("FieldMap: "+ mapFields);
				callGetAllSportsMeetups(mapFields);
				dialog.dismiss();
				//edtAgeLimit.setText( ageGroup[which] + "" );
				//edtAgeLimit.setTag( which + 1 );// setting tag as we need to send id of agegroup starting from 1-5
				//dialog.dismiss();
			}
		} );
		alert.show();
	}

	private void locationFilterPopup() {
		final Dialog dialog = new Dialog( context);
		dialog.requestWindowFeature( Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.popup_filter_location);
		final EditText edtCity      = (EditText ) dialog.findViewById( R.id.Popup_ET_city);
		Button         dialogButton = (Button) dialog.findViewById( R.id.Popup_BTN_filter);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(edtCity.getText().length()>0){
					Map<String, String> mapFields  = new HashMap<>();
					mapFields.put( "city", edtCity.getText().toString());
					context.oBaseApp.doLog("FieldMap: "+ mapFields);
					callGetAllSportsMeetups(mapFields);
					dialog.dismiss();
				}

			}
		});
		dialog.show();
	}


	private void timeFilterPopup() {
		final Dialog dialog = new Dialog( context);
		dialog.requestWindowFeature( Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.popup_filter_time);
		Button dialogButton = (Button) dialog.findViewById( R.id.Popup_BTN_filter);
		// if button is clicked, close the custom dialog
		final EditText edtTimeFrom = (EditText ) dialog.findViewById( R.id.Popup_ET_from );
		final EditText edtTimeTo   = (EditText ) dialog.findViewById( R.id.Popup_ET_to );

		edtTimeFrom.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick( View view ) {
				Calendar newCalendar = Calendar.getInstance();
				DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

					public void onDateSet( DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						Calendar newDate = Calendar.getInstance();
						newDate.set(year, monthOfYear, dayOfMonth);
						edtTimeFrom.setText(new SimpleDateFormat( "yyyy-MM-dd").format( newDate.getTime()));
					}

				},newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
				datePickerDialog.show();
			}
		} );

		edtTimeTo.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick( View view ) {
				Calendar newCalendar = Calendar.getInstance();
				DatePickerDialog datePickerDialog = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

					public void onDateSet( DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						Calendar newDate = Calendar.getInstance();
						newDate.set(year, monthOfYear, dayOfMonth);
						edtTimeTo.setText(new SimpleDateFormat( "yyyy-MM-dd").format( newDate.getTime()));
					}

				},newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
				datePickerDialog.show();
			}
		} );


		dialogButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if( NetworkUtil.isConnected( context )){
					if(edtTimeFrom.getText().toString().trim().length()>0){
						if(edtTimeTo.getText().toString().trim().length()>0){
							Map<String, String> mapFields  = new HashMap<>();
							mapFields.put( "from_date", edtTimeFrom.getText().toString());
							mapFields.put( "to_date", edtTimeTo.getText().toString());
							context.oBaseApp.doLog("FieldMap: "+ mapFields);
							callGetAllSportsMeetups(mapFields);
							dialog.dismiss();
						}else{
							context.oBaseApp.doToast("please select from date.");
						}
					}else{
						context.oBaseApp.doToast("please select to date.");
					}
				}else{
					context.oBaseApp.doToast( getString( R.string.msg_noInternet ) );
				}

			}
		});
		dialog.show();
	}
}
