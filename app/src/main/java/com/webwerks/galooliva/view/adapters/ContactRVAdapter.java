package com.webwerks.galooliva.view.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.network.model.Contact;
import com.webwerks.galooliva.utils.CircleTransform;
import com.webwerks.galooliva.view.activities.DetailsActivity;
import com.webwerks.galooliva.view.activities.HomeActivity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by webwerks on 19/12/16.
 */

public class ContactRVAdapter extends RecyclerView.Adapter<ContactRVAdapter.MyViewHolder> {

    private ArrayList<Contact> contactArrayList;
    private HomeActivity       context;

    public ContactRVAdapter( HomeActivity context, ArrayList<Contact> contactArrayList){
        this.context = context;
        this.contactArrayList = contactArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_contact_rv_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            context.oBaseApp.doToast( "Contact Clicked." );
            }
        });
        holder.tvName.setText( contactArrayList.get( position).getName());
        holder.tvPhone.setText( contactArrayList.get( position).getPhone());
	    if (contactArrayList.get( position ).getThumb() != null) {
		    //Picasso.with(context).load( contactArrayList.get( position ).getThumb()).resize( 90, 90).centerCrop().transform( new CircleTransform()).into( holder.imgThumb );
	        holder.imgThumb.setImageBitmap( contactArrayList.get( position ).getThumb() );
	    } else {
		    Picasso.with( context).load( R.mipmap.ic_launcher).resize( 90, 90).centerCrop().into( holder.imgThumb);
	    }
    }

    @Override
    public int getItemCount() {
        return contactArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName, tvPhone;
        public ImageView imgThumb;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById( R.id.ContactRVAdapter_TV_name);
            tvPhone = (TextView) view.findViewById( R.id.ContactRVAdapter_TV_phone);
            imgThumb = (ImageView) view.findViewById( R.id.ContactRVAdapter_IMG_thumb);
        }
    }
}
