package com.webwerks.galooliva.view.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.LoginResponse;
import com.webwerks.galooliva.data.network.model.StatusObject;
import com.webwerks.galooliva.utils.NetworkUtil;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

public class ChangePasswordActivity extends BaseActivity {

	private ChangePasswordActivity context; // context variable represents Activity scope
	private  Toolbar            toolbar;
	private  EditText            edtCurrentPassword, edtNewPassword,edtConfirmPassword;
	private Button btnChange;

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_change_password );
		init();
	}

	private void init() {
		context = this;
		initViews();
		initValues();
	}

	private void initViews() {
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		initToolbar(toolbar);
		setToolbarTitle(getString(R.string.changePassword));
		setHomeButtonEnabled();

		edtCurrentPassword = (EditText) findViewById(R.id.ChangePasswordActivity_ET_current_password);
		edtNewPassword = (EditText) findViewById(R.id.ChangePasswordActivity_ET_new_password);
		edtConfirmPassword = (EditText) findViewById(R.id.ChangePasswordActivity_ET_confirm_password);
		btnChange = (Button) findViewById(R.id.ChangePasswordActivity_BTN_change);
	}

	private void initValues() {
		btnChange.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick( View view ) {
				validateInputs();
			}
		} );
	}

	private void validateInputs() {
		if( NetworkUtil.isConnected( context )){
			if(edtCurrentPassword.getText().toString().trim().length()>0){
				if(edtNewPassword.getText().toString().trim().length()>0){
					if(edtNewPassword.getText().toString().matches( edtConfirmPassword.getText().toString() )){
						callChangePassword();
					}else{
						oBaseApp.doToast( "password does not match." );
					}
				}else{
					oBaseApp.doToast( "please enter new Password." );
				}
			}else{
				oBaseApp.doToast( "Please enter current password." );
			}
		}else{
			oBaseApp.doToast( getString( R.string.msg_noInternet ) );
		}
	}

	private void callChangePassword() {
		showProgress();
		RetroInterface apiService = oBaseApp.getClient().create( RetroInterface.class);

		Map<String, String> mapFields = new HashMap<>();
		mapFields.put("current_password", edtCurrentPassword.getText().toString().trim());
		mapFields.put("new_password", edtNewPassword.getText().toString().trim());
		mapFields.put("new_confirm_password", edtConfirmPassword.getText().toString().trim());
		oBaseApp.doLog("FieldMap: "+ mapFields);

		Call<StatusObject> call = apiService.postChangePassword( SharedPreferenceHelper.getAuthToken( context), mapFields );
		call.enqueue(new RetroCallback<StatusObject>( context) {
			@Override
			public void onSuccess(Call<StatusObject> call, Response<StatusObject> response) {
				if(response.body().getStatus() == 200){
					clearInputs();
				}
				oBaseApp.doToast(response.body().getUser_msg());

			}
		});
	}

	private void clearInputs() {
		edtCurrentPassword.setText( "" );
		edtNewPassword.setText( "" );
		edtConfirmPassword.setText( "" );
	}

}
