package com.webwerks.galooliva.view.activities;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.google.gson.Gson;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.LoginResponse;
import com.webwerks.galooliva.data.network.model.StatusObject;
import com.webwerks.galooliva.data.network.model.User;
import com.webwerks.galooliva.utils.Constants;
import com.webwerks.galooliva.utils.Util;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileEditActivity extends BaseActivity implements View.OnClickListener {

	private ProfileEditActivity context; // context variable represents Activity scope
	private Toolbar toolbar;
	private EditText edtFirstName, edtLastName,edtBirthDate, edtGender, edtCity, edtCountry;
	private Button btnSave;
	private DatePickerDialog datePickerDialog;
	private User user;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_edit);
		init();
	}

	private void init() {
		context = this;
		initViews();
		initValues();
	}

	private void initViews() {
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		initToolbar(toolbar);
		setToolbarTitle(getString(R.string.profileEdit));
		setHomeButtonEnabled();

		edtFirstName = (EditText) findViewById(R.id.ProfileEditActivity_ET_firstname);
		edtLastName = (EditText) findViewById(R.id.ProfileEditActivity_ET_lastname);
		edtBirthDate = (EditText) findViewById(R.id.ProfileEditActivity_ET_birthdate);
		edtGender = (EditText) findViewById(R.id.ProfileEditActivity_ET_gender);
		edtCity = (EditText) findViewById(R.id.ProfileEditActivity_ET_city);
		edtCountry = (EditText) findViewById(R.id.ProfileEditActivity_ET_country);
		btnSave = (Button) findViewById(R.id.ProfileEditActivity_BTN_save);
	}

	private void initValues() {
		btnSave.setOnClickListener(this);
		edtBirthDate.setOnClickListener(this);
		edtGender.setOnClickListener(this);

		callGetProfileService(); // get profile info from webservice
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()){
			case R.id.ProfileEditActivity_BTN_save:
				validateInputs();
				break;
			case R.id.ProfileEditActivity_ET_birthdate:
				showDateDialog();
				break;
			case R.id.ProfileEditActivity_ET_gender:
				setGender();
				break;
		}
		Util.closeEditor(context);
	}

	private void validateInputs() {
		if(edtFirstName.getText().toString().trim().length()>2){
			if(edtLastName.getText().toString().trim().length()>2){
				if(edtBirthDate.getText().toString().trim().length()>0){
					if(edtGender.getText().toString().trim().length()>0){
						if(edtCity.getText().toString().trim().length()>0){
							if(edtCountry.getText().toString().trim().length()>0) {
								callProfileEditService();
							}else{
								oBaseApp.doToast("Please enter country.");
							}
						}else{
							oBaseApp.doToast("Please enter city.");
						}
					}else{
						oBaseApp.doToast("Please select gender.");
					}
				}else{
					oBaseApp.doToast("Please set birth date.");
				}
			}else{
				oBaseApp.doToast("Please enter last name.");
			}
		}else{
			oBaseApp.doToast("Please enter first name.");
		}
	}

	public void setGender(){
		final CharSequence[] gender = {"Male","Female"};
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle("Select Gender");
		alert.setSingleChoiceItems(gender,-1, new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog, int which){
				edtGender.setText(gender[which]+"");
				dialog.dismiss();
			}
		});
		alert.show();
	}

	private void showDateDialog() {
		Calendar newCalendar = Calendar.getInstance();
		datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

			public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
				Calendar newDate = Calendar.getInstance();
				newDate.set(year, monthOfYear, dayOfMonth);
				edtBirthDate.setText(new SimpleDateFormat("yyyy-MM-dd").format(newDate.getTime()));
			}

		},newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
		datePickerDialog.show();
	}

	private void callGetProfileService() {
		showProgress();
		RetroInterface apiService = oBaseApp.getClient().create(RetroInterface.class);

		Call<LoginResponse> call = apiService.getProfileInfo(SharedPreferenceHelper.getAuthToken(context));
		call.enqueue(new RetroCallback<LoginResponse>(context) {
			@Override
			public void onSuccess(Call<LoginResponse> call, Response<LoginResponse> response) {
				user = response.body().getData();
				setValuesToViews(user);
			}
		});
	}

	private void setValuesToViews(User user) {
		edtFirstName.setText(user.getFirst_name());
		edtLastName.setText(user.getLast_name());
		edtBirthDate.setText(user.getDob());
		edtGender.setText((user.getGender() == 'M')?"Male":"Female");
		edtCity.setText(user.getCity());
		edtCountry.setText(user.getCountry());
	}

	private void callProfileEditService() {
		RetroInterface apiService = oBaseApp.getClient().create(RetroInterface.class);

		Map<String, String> mapFields = new HashMap<>();
		mapFields.put("first_name", edtFirstName.getText().toString().trim());
		mapFields.put("last_name", edtLastName.getText().toString().trim());
		mapFields.put("dob", edtBirthDate.getText().toString().trim());
		mapFields.put("gender", (edtGender.getText().toString().trim().equalsIgnoreCase("male"))?"M":"F");
		mapFields.put("email", user.getEmail());
		mapFields.put("city", edtCity.getText().toString().trim());
		mapFields.put("country", edtCountry.getText().toString().trim());
		oBaseApp.doLog("FieldMap: "+ mapFields);

		Call<LoginResponse> call = apiService.postProfileEdit(SharedPreferenceHelper.getAuthToken( context ),mapFields);
		call.enqueue(new RetroCallback<LoginResponse>(context) {
			@Override
			public void onSuccess(Call<LoginResponse> call, Response<LoginResponse> response) {
				oBaseApp.doToast(response.body().getUser_msg());
			}
		});
	}
}
