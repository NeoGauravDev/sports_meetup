package com.webwerks.galooliva.view.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.network.model.SportMeetupPhoto;

import java.util.ArrayList;

/**
 * Created by webwerks on 9/1/17.
 */

public class DetailsViewPagerAdapter extends PagerAdapter {

	Context        mContext;
	LayoutInflater mLayoutInflater;
	ArrayList< SportMeetupPhoto > sports_meet_up_photos;

	public DetailsViewPagerAdapter( Context context, ArrayList< SportMeetupPhoto > sports_meet_up_photos ) {
		mContext = context;
		mLayoutInflater = (LayoutInflater) mContext.getSystemService( Context.LAYOUT_INFLATER_SERVICE);
		this.sports_meet_up_photos = sports_meet_up_photos;
	}

	@Override
	public int getCount() {
		return sports_meet_up_photos.size();
	}

	@Override
	public boolean isViewFromObject( View view, Object object) {
		return view == ((LinearLayout ) object);
	}

	@Override
	public Object instantiateItem( ViewGroup container, int position) {
		View itemView = mLayoutInflater.inflate( R.layout.activity_details_page_adapter, container, false);

		ImageView imageView = (ImageView) itemView.findViewById( R.id.DetailsViewPagerAdapter_img);

		 if(sports_meet_up_photos.size()>0){
		    Picasso.with( mContext).load( sports_meet_up_photos.get( position ).getImage_name()).resize( 300, 400 ).centerCrop().into( imageView);
	    }else{
		    Picasso.with(mContext).load(R.mipmap.ic_launcher).centerInside().resize( 100,100 ).into(imageView);
	    }

		container.addView(itemView);
		return itemView;
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		container.removeView((LinearLayout) object);
	}
}
