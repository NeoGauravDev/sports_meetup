package com.webwerks.galooliva.view.fragments;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.DeleteFCMTokenService;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.model.StatusObject;
import com.webwerks.galooliva.view.activities.AdditionalInfoActivity;
import com.webwerks.galooliva.view.activities.ChangePasswordActivity;
import com.webwerks.galooliva.view.activities.HomeActivity;
import com.webwerks.galooliva.view.activities.LoginActivity;
import com.webwerks.galooliva.view.activities.NotificationActivity;
import com.webwerks.galooliva.view.activities.ProfileEditActivity;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.User;
import com.webwerks.galooliva.utils.Constants;
import com.webwerks.galooliva.utils.NetworkUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by webwerks on 21/12/16.
 */

public class SettingsFragment extends Fragment implements View.OnClickListener{

    private HomeActivity context; // context variable represents Activity scope
    private View rootView;
    private TextView tvProfileEdit, tvAdditionalInfo, tvNotification, tvLogout , tvChangePassword;
    private Intent intent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_settings, container,false);
        init();
        return rootView;
    }

    private void init() {
        context = (HomeActivity) getActivity();
        initViews();
        initValues();
    }

    private void initViews() {
        tvProfileEdit = (TextView) rootView.findViewById(R.id.SettingsFragment_TV_profileEdit);
        tvAdditionalInfo = (TextView) rootView.findViewById(R.id.SettingsFragment_TV_additional_info);
	    tvChangePassword = (TextView) rootView.findViewById(R.id.SettingsFragment_TV_change_password);
        tvNotification= (TextView) rootView.findViewById(R.id.SettingsFragment_TV_notification);
        tvLogout= (TextView) rootView.findViewById(R.id.SettingsFragment_TV_logout);
    }

    private void initValues() {
        tvProfileEdit.setOnClickListener(this);
        tvAdditionalInfo.setOnClickListener(this);
        tvChangePassword.setOnClickListener(this);
        tvNotification.setOnClickListener(this);
        tvLogout.setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        context.setToolbarTitle(getString(R.string.settings));
        context.setHomeButtonEnabled();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater){
        super.onCreateOptionsMenu(menu, inflater);
        menu.findItem(R.id.action_filter).setVisible(false);
        menu.findItem(R.id.action_notification).setVisible(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.SettingsFragment_TV_profileEdit:
                intent = new Intent(context, ProfileEditActivity.class);
                startActivity(intent);
                break;
	        case R.id.SettingsFragment_TV_additional_info:
		        buildDialog(R.style.DialogTheme);
		        break;
	        case R.id.SettingsFragment_TV_change_password:
		        intent = new Intent(context, ChangePasswordActivity.class);
		        startActivity(intent);
		        break;
            case R.id.SettingsFragment_TV_notification:
                intent = new Intent(context, NotificationActivity.class);
                startActivity(intent);
                break;
            case R.id.SettingsFragment_TV_logout:
                if(NetworkUtil.isConnected(context)){
                    callLogoutService();
                }else{
                    context.oBaseApp.doToast(getString(R.string.msg_noInternet));
                }
                break;
        }
    }

	private void buildDialog(int animationSource) {
		final Dialog dialog = new Dialog(context,android.R.style.Theme_Black_NoTitleBar_Fullscreen);
		dialog.requestWindowFeature( Window.FEATURE_NO_TITLE);
		dialog.setContentView( R.layout.activity_splash );
		dialog.setCancelable( false );
		RelativeLayout layout = ( RelativeLayout ) dialog.findViewById( R.id.activity_splash );
		layout.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick( View view ) {
				dialog.dismiss();
				intent = new Intent(context, AdditionalInfoActivity.class);
				startActivity(intent);
			}
		} );
		dialog.getWindow().getAttributes().windowAnimations = animationSource;
		dialog.show();


	}

    private void callLogoutService() {
        context.showProgress();

        RetroInterface apiService = context.oBaseApp.getClient().create(RetroInterface.class);
        Call<StatusObject> call = apiService.getLogout(SharedPreferenceHelper.getAuthToken(context));
        call.enqueue(new RetroCallback<StatusObject>(context) {
            @Override
            public void onSuccess(Call<StatusObject> call, Response<StatusObject> response) {
                try {
                    StatusObject statusObject = response.body();
                    context.oBaseApp.doToast(statusObject.getUser_msg());
                    SharedPreferenceHelper.setisSignIn(context,false);
	                context.startService(new Intent(context, DeleteFCMTokenService.class));
                    context.finish();
                    intent = new Intent(context,LoginActivity.class);
                    startActivity(intent);
                }catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
