package com.webwerks.galooliva.view.adapters;

import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by webwerks on 20/1/17.
 */

public class GoogleAutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {

	private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place/autocomplete/json";
	private static final String API_KEY = "AIzaSyBU3DCOKnS10RLGXuW8yJDyW_xD5xZF6R8";

	private ArrayList<String> resultList;

	public GoogleAutoCompleteAdapter( Context context, int textViewResourceId) {
		super(context, textViewResourceId);
	}

	@Override
	public int getCount() {
		return resultList.size();
	}

	@Override
	public String getItem(int index) {
		return resultList.get(index);
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {
			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults filterResults = new FilterResults();
				if (constraint != null) {
					// Retrieve the autocomplete results.
					resultList = autocomplete(constraint.toString());

					// Assign the data to the FilterResults
					filterResults.values = resultList;
					filterResults.count = resultList.size();
				}
				return filterResults;
			}

			@Override
			protected void publishResults(CharSequence constraint, FilterResults results) {
				if (results != null && results.count > 0) {
					notifyDataSetChanged();
				} else {
					notifyDataSetInvalidated();
				}
			}
		};
		return filter;
	}


	public static ArrayList<String> autocomplete(String input) {
		ArrayList<String> resultList = null;

		HttpURLConnection conn        = null;
		StringBuilder     jsonResults = new StringBuilder();
		try {
			StringBuilder sb = new StringBuilder(PLACES_API_BASE );
			sb.append("?key=" + API_KEY);
			//sb.append("&components=country:gr");
			//sb.append("&sensor=false"); //&types=(cities)
			sb.append("&input=" + URLEncoder.encode( input, "utf8"));

			URL url = new URL( sb.toString());

			System.out.println("URL: "+url);
			conn = (HttpURLConnection) url.openConnection();
			InputStreamReader in = new InputStreamReader( conn.getInputStream());

			// Load the results into a StringBuilder
			int read;
			char[] buff = new char[1024];
			while ((read = in.read(buff)) != -1) {
				jsonResults.append(buff, 0, read);
			}
		} catch (MalformedURLException e) {
			Log.e( "", "Error processing Places API URL", e);
			return resultList;
		} catch (IOException e) {
			Log.e("", "Error connecting to Places API", e);
			return resultList;
		} finally {
			if (conn != null) {
				conn.disconnect();
			}
		}

		try {

			// Create a JSON object hierarchy from the results
			JSONObject jsonObj        = new JSONObject( jsonResults.toString());
			JSONArray  predsJsonArray = jsonObj.getJSONArray( "predictions");

			// Extract the Place descriptions from the results
			resultList = new ArrayList<String>(predsJsonArray.length());
			for (int i = 0; i < predsJsonArray.length(); i++) {
				System.out.println(predsJsonArray.getJSONObject(i).getString("description"));
				String strCity="";
				//System.out.println("============================================================");
				if(predsJsonArray.getJSONObject(i).getString("description").contains( "," )) {
					strCity = predsJsonArray.getJSONObject( i ).getString( "description" ).substring( 0, predsJsonArray.getJSONObject( i ).getString( "description" ).indexOf( "," ) );
				}else{
					strCity = predsJsonArray.getJSONObject( i ).getString( "description" );
				}
				System.out.println(strCity);
				resultList.add(strCity);
			}
		} catch (JSONException e) {
			Log.e("", "Cannot process JSON results", e);
		}

		return resultList;
	}
}
