package com.webwerks.galooliva.view.adapters;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.Notification;
import com.webwerks.galooliva.data.network.model.NotificationResponse;
import com.webwerks.galooliva.data.network.model.StatusObject;
import com.webwerks.galooliva.view.activities.BaseActivity;
import com.webwerks.galooliva.view.activities.HomeActivity;
import com.webwerks.galooliva.view.activities.NotificationActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by webwerks on 19/12/16.
 */

public class NotificationRVAdapter extends RecyclerView.Adapter<NotificationRVAdapter.MyViewHolder> {

    private ArrayList<Notification> notificationArrayList;
    private NotificationActivity    context;

    public NotificationRVAdapter( NotificationActivity context, ArrayList<Notification> notificationArrayList ){
        this.context = context;
        this.notificationArrayList = notificationArrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_notification_rv_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

	    // to check whether notification is read or not
	    if( notificationArrayList.get( position ).is_read()){
		    holder.itemView.setBackgroundColor( Color.parseColor("#dbdbdb") );
	    }else{
		    holder.itemView.setBackgroundColor( Color.WHITE );
	    }

        holder.tvTitle.setText( notificationArrayList.get( position).getTitle());
        holder.tvDesc.setText( notificationArrayList.get( position).getSubtitle());

	    // to check whether notification is "join_closed" or not
	    if(notificationArrayList.get( position ).getNotification_type().equalsIgnoreCase( "join_closed" )){
			holder.imgAccept.setVisibility( View.VISIBLE );
			holder.imgReject.setVisibility( View.VISIBLE );
	    }else{
		    holder.imgAccept.setVisibility( View.GONE );
		    holder.imgReject.setVisibility( View.GONE );
	    }

	    holder.imgAccept.setOnClickListener( new View.OnClickListener() {
		    @Override
		    public void onClick( View view ) {
				callAcceptJoinRequest("yes", position);
		    }
	    } );

	    holder.imgReject.setOnClickListener( new View.OnClickListener() {
		    @Override
		    public void onClick( View view ) {
			    callAcceptJoinRequest("no", position);
		    }
	    } );

	    holder.imgRemove.setOnClickListener( new View.OnClickListener() {
		    @Override
		    public void onClick( View view ) {
				callDeleteNotification( position );
		    }
	    } );

    }

	@Override
    public int getItemCount() {
        return notificationArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvDesc;
        public ImageView imgAccept, imgReject, imgRemove;

        public MyViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.NotificationRVAdapter_TXT_title);
            tvDesc = (TextView) view.findViewById( R.id.NotificationRVAdapter_TXT_subTitle);
	        imgAccept = (ImageView) view.findViewById(R.id.NotificationRVAdapter_IMG_accept);
	        imgReject = (ImageView) view.findViewById(R.id.NotificationRVAdapter_IMG_reject);
	        imgRemove = (ImageView) view.findViewById(R.id.NotificationRVAdapter_IMG_remove);
        }
    }

	private void callDeleteNotification( final int position) {
		context.showProgress();
		RetroInterface             apiService = context.oBaseApp.getClient().create( RetroInterface.class);

		Map<String, String> mapFields = new HashMap<>();
		mapFields.put("notification_detail_id", notificationArrayList.get( position ).getId()+"");
		context.oBaseApp.doLog("FieldMap: "+ mapFields);

		Call<StatusObject> call = apiService.postDeleteNotification( SharedPreferenceHelper.getAuthToken( context), mapFields);
		call.enqueue(new RetroCallback<StatusObject>( context) {
			@Override
			public void onSuccess(Call<StatusObject> call, Response<StatusObject> response) {
				notificationArrayList.remove( position );
				notifyDataSetChanged();
			}
		});
	}

	private void callAcceptJoinRequest( String res ,final int position) {
		context.showProgress();
		RetroInterface apiService = context.oBaseApp.getClient().create( RetroInterface.class );
		Map< String, String > mapFields = new HashMap<>(  );
		try {
			JSONObject jsonObject = new JSONObject( notificationArrayList.get( position ).getNotification_body() );
			JSONObject info = jsonObject.getJSONObject( "body" ).getJSONObject( "info" );
			context.oBaseApp.doLog( "Json: " + info );
			mapFields.put( "sports_meet_up_detail_id", info.getJSONObject( "joining_sports_meetup_user" ).getString( "sports_meet_up_detail_id" ) );
			mapFields.put( "sports_meet_up_user_id", info.getJSONObject( "joining_sports_meetup_user" ).getString( "id" ) );
			mapFields.put( "is_approved", res );
			context.oBaseApp.doLog( "FieldMap: " + mapFields );

		}
		catch ( JSONException e ) {
			e.printStackTrace();
		}

		Call< StatusObject > call = apiService.postAcceptJoinRequest( SharedPreferenceHelper.getAuthToken( context ), mapFields );
		call.enqueue( new RetroCallback< StatusObject >( context ) {
			@Override
			public void onSuccess( Call< StatusObject > call, Response< StatusObject > response ) {
				context.oBaseApp.doToast( response.body().getUser_msg() );
			}
		} );
	}
}
