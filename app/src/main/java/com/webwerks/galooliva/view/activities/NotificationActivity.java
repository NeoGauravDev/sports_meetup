package com.webwerks.galooliva.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.NotificationResponse;
import com.webwerks.galooliva.view.adapters.NotificationRVAdapter;

import retrofit2.Call;
import retrofit2.Response;

import static android.support.v7.recyclerview.R.attr.layoutManager;

public class NotificationActivity extends BaseActivity {

    private NotificationActivity context; // context variable represents Activity scope
    private Toolbar toolbar;

	private NotificationRVAdapter notificationRVAdapter;
	private RecyclerView          recyclerView;
	private NotificationResponse notificationResponse = null;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        init();
    }

    private void init() {
        context = this;
        initViews();
        initValues();
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        initToolbar(toolbar);
        setToolbarTitle(getString(R.string.notifications));
        setHomeButtonEnabled();

	    recyclerView = (RecyclerView) findViewById(R.id.NotificationActivity_RV );
    }

    private void initValues() {
		callGetAllNotifications();
    }

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		switch (id){
			case android.R.id.home:
				Bundle bundle = getIntent().getExtras();
				if(bundle !=null){
					Intent intent =new Intent( context, HomeActivity.class );
					startActivity( intent );
				}else{
					super. onBackPressed();
				}
				break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void callGetAllNotifications() {
		context.showProgress();
		RetroInterface              apiService = context.oBaseApp.getClient().create( RetroInterface.class);
		Call<NotificationResponse> call       = apiService.getAllNotifications( SharedPreferenceHelper.getAuthToken( context));
		call.enqueue(new RetroCallback<NotificationResponse>( context) {
			@Override
			public void onSuccess(Call<NotificationResponse> call, Response<NotificationResponse> response) {
				try{
					notificationResponse  = response.body();
					if(notificationResponse.getData() != null){
						notificationRVAdapter = new NotificationRVAdapter( context, notificationResponse.getData());
						RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
						recyclerView.setLayoutManager(mLayoutManager);
						/*DividerItemDecoration dividerItemDecoration = new DividerItemDecoration( recyclerView.getContext(),1);
						recyclerView.addItemDecoration(dividerItemDecoration);*/
						recyclerView.setItemAnimator(new DefaultItemAnimator());
						recyclerView.setAdapter( notificationRVAdapter );
					}

				}catch (Exception e){
					e.printStackTrace();
				}
			}
		});
	}

}
