package com.webwerks.galooliva.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.StatusObject;
import com.webwerks.galooliva.utils.NetworkUtil;
import com.webwerks.galooliva.view.fragments.ContactUsFragment;
import com.webwerks.galooliva.view.fragments.CreateMeetupFragment;
import com.webwerks.galooliva.view.fragments.GroupsFragment;
import com.webwerks.galooliva.view.fragments.MeetUpListFragment;
import com.webwerks.galooliva.view.fragments.MyMeetUpsFragment;
import com.webwerks.galooliva.view.fragments.SettingsFragment;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

public class HomeActivity extends BaseActivity implements View.OnClickListener {

    private HomeActivity context; // context variable represents Activity scope
    private Toolbar toolbar;
    private RadioGroup radioGroupBottom;
    private RadioButton rdbContactUs, rdbGroups, rdbCreate, rdbMeetUps, rdbSettings;

    public FrameLayout frameLayoutContainer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
    }

    private void init() {
        context = this;
        initViews();
        initValues();
    }

    private void initViews() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        initToolbar(toolbar);

        radioGroupBottom = (RadioGroup) findViewById(R.id.HomeActivity_RG_bottom);
        frameLayoutContainer = (FrameLayout) findViewById(R.id.HomeActivity_FL_container);

        rdbContactUs = (RadioButton) findViewById(R.id.HomeActivity_RDB_contactus);
        rdbGroups = (RadioButton) findViewById(R.id.HomeActivity_RDB_groups);
        rdbCreate = (RadioButton) findViewById(R.id.HomeActivity_RDB_create);
        rdbMeetUps = (RadioButton) findViewById(R.id.HomeActivity_RDB_meetups);
        rdbSettings = (RadioButton) findViewById(R.id.HomeActivity_RDB_settings);
    }

    private void initValues() {
        context.doReplaceFragment(new MeetUpListFragment(), frameLayoutContainer, "");

        rdbContactUs.setOnClickListener(this);
        rdbGroups.setOnClickListener(this);
        rdbCreate.setOnClickListener(this);
        rdbMeetUps.setOnClickListener(this);
        rdbSettings.setOnClickListener(this);
	    callSendFCMToken(); // sending FCM Token from background
    }

    public void unselectedTabs(){
        if(radioGroupBottom.getCheckedRadioButtonId() != -1){
            ((RadioButton)findViewById(radioGroupBottom.getCheckedRadioButtonId())).setChecked(false);
        }
    }

    public void setselectedTab(int position){
        ((RadioButton)findViewById(radioGroupBottom.getChildAt(position).getId())).setChecked(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
	            oBaseApp.doLog( "Loading MeetUpList Fragment" );
                context.doStackFragment(new MeetUpListFragment(), frameLayoutContainer, "");
                break;
            case R.id.action_notification:
                Intent intent = new Intent(context, NotificationActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.HomeActivity_RDB_contactus://contact
                context.doStackFragment(new ContactUsFragment(), frameLayoutContainer, "");
                break;
            case R.id.HomeActivity_RDB_groups://groups
                context.doStackFragment(new GroupsFragment(), frameLayoutContainer, "");
                break;
            case R.id.HomeActivity_RDB_create://create
	            CreateMeetupFragment createMeetupFragment =new CreateMeetupFragment();
	            Bundle bundle= new Bundle(  );
	            bundle.putSerializable( "meetup", null);
	            createMeetupFragment.setArguments( bundle );
	            context.doStackFragment( createMeetupFragment,context.frameLayoutContainer, "");
                //context.doStackFragment(new CreateMeetupFragment(), frameLayoutContainer, "");
                break;
            case R.id.HomeActivity_RDB_meetups://meetups
                context.doStackFragment(new MyMeetUpsFragment(), frameLayoutContainer, "");
                break;
            case R.id.HomeActivity_RDB_settings://settings
                context.doStackFragment(new SettingsFragment(), frameLayoutContainer, "");
                break;
        }
        /*int count = radioGroupBottom.getChildCount();
        for (int x=0;x<count;x++) {
            Drawable[] drawableNonSelected = ((RadioButton)findViewById(radioGroupBottom.getChildAt(x).getId())).getCompoundDrawables();
            drawableNonSelected[1].setColorFilter(getResources().getColor(R.color.colorGray), PorterDuff.Mode.SRC_ATOP);
        }

        Drawable[] drawable = ((RadioButton)findViewById(radioGroupBottom.getCheckedRadioButtonId())).getCompoundDrawables();
        drawable[1].setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP); //  drawable[1] represents topdrawable*/
    }

	public void callSendFCMToken(){
		if( NetworkUtil.isConnected( context )){
			if(! SharedPreferenceHelper.isFCMSent( context )){ // checking if false then send to server
				if(SharedPreferenceHelper.getFcmToken( context ).length()>0) {
					RetroInterface        apiService = context.oBaseApp.getClient().create( RetroInterface.class );
					Map< String, String > mapFields  = new HashMap<>();
					mapFields.put( "device_token", SharedPreferenceHelper.getFcmToken( context ) );
					mapFields.put( "device_type", "android" );
					context.oBaseApp.doLog( "FieldMap: " + mapFields );
					Call< StatusObject > call = apiService.postFCMToken( SharedPreferenceHelper.getAuthToken( context ), mapFields );
					call.enqueue( new RetroCallback< StatusObject >( context ) {
						@Override
						public void onSuccess( Call< StatusObject > call, Response< StatusObject > response ) {
							SharedPreferenceHelper.setisFCMSent( context, true );
							oBaseApp.doLog( "FCM Sent" );
						}
					} );
				}
			}
		}
	}
}
