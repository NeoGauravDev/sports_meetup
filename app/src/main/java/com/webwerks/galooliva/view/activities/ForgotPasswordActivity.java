package com.webwerks.galooliva.view.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.StatusObject;
import com.webwerks.galooliva.utils.Util;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

public class ForgotPasswordActivity extends BaseActivity {
    private ForgotPasswordActivity context; // context variable represents Activity scope
    private EditText edtEmail;
    private Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        init();
    }

    private void init() {
        context = this;
        iniViews();
        initValues();
    }

    private void iniViews() {
        edtEmail = (EditText) findViewById(R.id.ForgotPasswordActivity_ET_email);
        btnSend = (Button) findViewById(R.id.ForgotPasswordActivity_BTN_send);
    }

    private void initValues() {
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Util.validateEmail(edtEmail.getText().toString().trim())){
                    callForgotPasswordService();
                }else{
                    oBaseApp.doToast("Please enter valid email.");
                }
            }
        });
    }

    private void callForgotPasswordService() {
        RetroInterface apiService = oBaseApp.getClient().create(RetroInterface.class);

        Map<String, String> mapFields = new HashMap<>();
        mapFields.put("email", edtEmail.getText().toString().trim());
        oBaseApp.doLog("FieldMap: "+ mapFields);

        Call<StatusObject> call = apiService.postForgotPassword(mapFields);
        call.enqueue(new RetroCallback<StatusObject>(context) {
            @Override
            public void onSuccess(Call<StatusObject> call, Response<StatusObject> response) {
                oBaseApp.doToast(response.body().getUser_msg());
            }
        });
    }
}
