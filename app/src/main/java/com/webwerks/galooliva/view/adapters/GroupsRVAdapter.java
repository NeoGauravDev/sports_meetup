package com.webwerks.galooliva.view.adapters;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.network.model.MeetUp;
import com.webwerks.galooliva.utils.CircleTransform;
import com.webwerks.galooliva.view.activities.DetailsActivity;
import com.webwerks.galooliva.view.activities.GroupChatActivity;
import com.webwerks.galooliva.view.activities.HomeActivity;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by webwerks on 21/12/16.
 */

public class GroupsRVAdapter extends RecyclerView.Adapter<GroupsRVAdapter.MyViewHolder> {

    private ArrayList<MeetUp> meetUpArrayList;
    private HomeActivity      context;

    public GroupsRVAdapter(HomeActivity context, ArrayList<MeetUp> meetUpArrayList){
        this.context = context;
        this.meetUpArrayList = meetUpArrayList;
    }

    @Override
    public GroupsRVAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_groups_rv_adapter, parent, false);

        return new GroupsRVAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder( GroupsRVAdapter.MyViewHolder holder, final int position) {

	    holder.itemView.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    Intent intent =new Intent(context, DetailsActivity.class);
			    intent.putExtra("meetup", (Serializable ) meetUpArrayList.get( position));
			    intent.putExtra("isFromGroup", true);
			    context.startActivity(intent);
		    }
	    });

	    holder.tvTitle.setText(meetUpArrayList.get(position).getTitle());
	    holder.tvDesc.setText(meetUpArrayList.get(position).getDescription());
	    holder.tvStatus.setText( meetUpArrayList.get(position).getMeetup_type() );

	    if(meetUpArrayList.get(position).getSports_meet_up_photos().size()>0){
		    Picasso.with(context).load(meetUpArrayList.get(position).getSports_meet_up_photos().get(0).getImage_name()).resize(90,90).centerCrop().into(holder.img);
	    }else{
		    Picasso.with(context).load(R.mipmap.ic_launcher).resize(90,90).centerCrop().into(holder.img);
	    }

	    holder.imgChat.setOnClickListener( new View.OnClickListener() {
		    @Override
		    public void onClick( View view ) {
			    Intent intent = new Intent( context, GroupChatActivity.class );
			    context.startActivity( intent );
		    }
	    } );
    }

    @Override
    public int getItemCount() {
        return meetUpArrayList.size();
    }

	public class MyViewHolder extends RecyclerView.ViewHolder {
		public TextView tvTitle, tvDesc , tvStatus;
		ImageView img, imgCreate, imgChat;

		public MyViewHolder(View view) {
			super(view);
			tvTitle = (TextView) view.findViewById( R.id.GroupsRVAdapter_TV_title);
			tvDesc = (TextView) view.findViewById( R.id.GroupsRVAdapter_TV_description);
			tvStatus = (TextView) view.findViewById( R.id.GroupsRVAdapter_TV_status);
			img = (ImageView) view.findViewById(R.id.GroupsRVAdapter_IMG);
			imgCreate = (ImageView) view.findViewById(R.id.GroupsRVAdapter_IMG_create );
			imgChat = (ImageView) view.findViewById(R.id.GroupsRVAdapter_IMG_chat );
		}
	}
}

