package com.webwerks.galooliva.view.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.webwerks.galooliva.R;
import com.webwerks.galooliva.data.local.SharedPreferenceHelper;
import com.webwerks.galooliva.data.network.RetroCallback;
import com.webwerks.galooliva.data.network.RetroInterface;
import com.webwerks.galooliva.data.network.model.AdditionalInfo;
import com.webwerks.galooliva.data.network.model.LoginResponse;
import com.webwerks.galooliva.utils.NetworkUtil;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

public class AdditionalInfoEditActivity extends BaseActivity {

	private AdditionalInfoEditActivity context; // context variable represents Activity scope
	private Toolbar                    toolbar;
	private TextView                   edtHeight, edtWeight, edtDominantHand, edtBirthPlace, edtMyInterest, edtGoals;
	private Button btnSave;
	private AdditionalInfo additionalInfo;

	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_additional_info_edit );
		init();
	}

	private void init() {
		context = this;
		initViews();
		initValues();
	}

	private void initViews() {
		toolbar = (Toolbar) findViewById(R.id.toolbar);
		initToolbar(toolbar);
		setToolbarTitle(getString(R.string.additionalInfo));
		setHomeButtonEnabled();

		edtHeight = (TextView ) findViewById( R.id.AdditionalInfoEditActivity_ET_height );
		edtWeight = (TextView ) findViewById( R.id.AdditionalInfoEditActivity_ET_weight );
		edtDominantHand = (TextView ) findViewById( R.id.AdditionalInfoEditActivity_ET_dominant_hand );
		edtBirthPlace = (TextView ) findViewById( R.id.AdditionalInfoEditActivity_ET_birthplace );
		edtMyInterest = (TextView ) findViewById( R.id.AdditionalInfoEditActivity_ET_my_interest );
		edtGoals = (TextView ) findViewById( R.id.AdditionalInfoEditActivity_ET_goals );

		btnSave = (Button) findViewById(R.id.AdditionalInfoEditActivity_BTN_save);
	}

	private void initValues() {
		additionalInfo = ( AdditionalInfo ) getIntent().getExtras().getSerializable( "additionalInfo" );
		if(additionalInfo != null){
			edtHeight.setText( additionalInfo.getHeight()+"");
			edtWeight.setText( additionalInfo.getWeight()+"" );
			edtDominantHand.setText( additionalInfo.getDominant_hand() );
			edtBirthPlace.setText( additionalInfo.getBirth_place() );
			edtMyInterest.setText( additionalInfo.getMy_interest() );
			edtGoals.setText( additionalInfo.getGoals_or_synonyms() );
		}

		btnSave.setOnClickListener( new View.OnClickListener() {
			@Override
			public void onClick( View view ) {
				if( NetworkUtil.isConnected( context )) {
					callEditAddtioanalInfo();
				}else{
					oBaseApp.doToast(getString( R.string.msg_noInternet ));
				}
			}
		} );
	}

	private void callEditAddtioanalInfo() {
		showProgress();
		RetroInterface apiService = oBaseApp.getClient().create( RetroInterface.class);

		Map<String, String> mapFields = new HashMap<>();
		if(edtHeight.getText().toString().trim().length()>0)
			mapFields.put( "height", edtHeight.getText().toString().trim());
		if(edtWeight.getText().toString().trim().length()>0)
			mapFields.put( "weight", edtWeight.getText().toString().trim());
		if(edtDominantHand.getText().toString().trim().length()>0)
			mapFields.put( "dominant_hand", edtDominantHand.getText().toString().trim());
		if(edtBirthPlace.getText().toString().trim().length()>0)
			mapFields.put( "birth_place", edtBirthPlace.getText().toString().trim());
		if(edtMyInterest.getText().toString().trim().length()>0)
			mapFields.put( "my_interest", edtMyInterest.getText().toString().trim());
		if(edtGoals.getText().toString().trim().length()>0)
			mapFields.put( "goals_or_synonyms", edtGoals.getText().toString().trim());
		oBaseApp.doLog("FieldMap: "+ mapFields);

		Call<LoginResponse> call = apiService.postEditAdditionalInfo( SharedPreferenceHelper.getAuthToken( context ), mapFields);
		call.enqueue(new RetroCallback<LoginResponse>( context) {
			@Override
			public void onSuccess(Call<LoginResponse> call, Response<LoginResponse> response) {
				oBaseApp.doToast(response.body().getUser_msg());
				finish();
			}
		});
	}
}
