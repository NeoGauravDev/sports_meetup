package com.webwerks.galooliva.view.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.webwerks.galooliva.R;

public class GroupChatActivity extends BaseActivity {

	private GroupChatActivity context; // context variable represents Activity scope
	private Toolbar toolbar;
	@Override
	protected void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		setContentView( R.layout.activity_group_chat );
		init();
	}

	private void init() {
		context = this;
		initViews();
		initValues();
	}

	private void initViews() {
		toolbar = ( Toolbar ) findViewById( R.id.toolbar );
		initToolbar( toolbar );
		setToolbarTitle( getString( R.string.meetupGroup ) );
		setHomeButtonEnabled();
	}

	private void initValues() {
	}
}
